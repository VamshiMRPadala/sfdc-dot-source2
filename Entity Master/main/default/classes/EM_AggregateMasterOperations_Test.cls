@isTest
public class EM_AggregateMasterOperations_Test {
    @isTest
    static void myTest() {
        set<id> RecordId = new Set<Id>();
        EM_AggregateMaster__c amc = new EM_AggregateMaster__c();
        amc.EM_AladdinIDRequiredFlag__c = 'No';
        amc.EM_BenchmarkRequiredFlag__c = 'No';
        amc.EM_BoardConsultantAggregate__c = false;
        amc.EM_AggregateReportingName__c = 'fdsgh';
        amc.EM_Status__c ='Pending Opening';
        //amc.EM_Execute__c = true;
        amc.EM_AggregateEntityCode__c = 'entitytest1';
        amc.EM_RecordStatus__c='Pending Implementation';
        insert amc;
        RecordID.add(amc.id);
        // code_block
        EM_AggregateMaster__c am = new EM_AggregateMaster__c();
        //am.EM_AggregateEntityCode__c,EM_AggregateLevel__c,EM_AggregateNotes__c,EM_AggregateRelationship__c,EM_AggregateReportingName__c,EM_AggregateRuleDescription__c,EM_AggregateRuleEffectiveDate__c,EM_AggregateRule__c,EM_AladdinIDRequiredFlag__c,EM_AladdinID__c,EM_BenchmarkRequiredFlag__c,EM_BoardConsultantAggregate__c,EM_ClosureDate__c,EM_ComponentListStatus__c,EM_EffectiveDate__c,EM_EntityTypeCode__c,EM_EntityTypeDescription__c,EM_FileSubmissionDate__c,EM_ICAStatus__c,EM_IncentiveCompensationAggregate__c,EM_InceptionDate__c,EM_PerformanceEndDate__c,EM_PerformanceNotes__c,EM_PerformanceStartDate__c,EM_PortfolioRuleDescription__c,EM_PrimaryBenchmarkEffectiveDate__c,EM_PrimaryBenchmarkNumber__c,EM_PrimaryBenchmarkReturnType__c,EM_PrimaryOfficialBenchmarkID__c,EM_PrimaryOfficialBenchmarkName__c,EM_RecordStatus__c,EM_RuleExecutionDateTime__c,EM_SSBBenchmarkID__c,EM_SSBEntityCode__c,EM_Status__c,EM_Trust__c,Id,Name,OwnerId,RecordTypeId
        am.EM_AggregateRuleDescription__c = 'EM_Trust__c = \'HCF\'';
        //am.EM_AggregateRule__c = 'Portfolio = EM_SsbPortfolioStatus__c = \'Actived\' &  Aggregate = EM_Trust__c = \'HCGF\'';
        am.EM_AladdinIDRequiredFlag__c = 'No';
        am.EM_BenchmarkRequiredFlag__c = 'No';
        am.EM_BoardConsultantAggregate__c = false;
        am.EM_AggregateEntityCode__c = 'entitytest2';
        am.EM_EntityTypeDescription__c = 'Governance Aggregate';
        am.EM_AggregateReportingName__c = 'fdhknfhgh';
        //am.EM_Execute__c = false;
        am.EM_IncentiveCompensationAggregate__c = 'No';
        am.EM_PortfolioRuleDescription__c = 'EM_SsbPortfolioStatus__c = \'Active\'';
        am.EM_RuleExecutionDateTime__c = system.now();
        am.EM_Status__c ='Pending Opening';
        am.EM_RecordStatus__c='Pending Implementation';
        Id prid = Schema.SObjectType.EM_AggregateMaster__c.getRecordTypeInfosByName()
            .get('Aggregate Master')
            .getRecordTypeId();
        am.RecordTypeId = prid;
        insert am;
        
       
        
        EM_AggregateMaster__c am1 = new EM_AggregateMaster__c();
        //am.EM_AggregateEntityCode__c,EM_AggregateLevel__c,EM_AggregateNotes__c,EM_AggregateRelationship__c,EM_AggregateReportingName__c,EM_AggregateRuleDescription__c,EM_AggregateRuleEffectiveDate__c,EM_AggregateRule__c,EM_AladdinIDRequiredFlag__c,EM_AladdinID__c,EM_BenchmarkRequiredFlag__c,EM_BoardConsultantAggregate__c,EM_ClosureDate__c,EM_ComponentListStatus__c,EM_EffectiveDate__c,EM_EntityTypeCode__c,EM_EntityTypeDescription__c,EM_FileSubmissionDate__c,EM_ICAStatus__c,EM_IncentiveCompensationAggregate__c,EM_InceptionDate__c,EM_PerformanceEndDate__c,EM_PerformanceNotes__c,EM_PerformanceStartDate__c,EM_PortfolioRuleDescription__c,EM_PrimaryBenchmarkEffectiveDate__c,EM_PrimaryBenchmarkNumber__c,EM_PrimaryBenchmarkReturnType__c,EM_PrimaryOfficialBenchmarkID__c,EM_PrimaryOfficialBenchmarkName__c,EM_RecordStatus__c,EM_RuleExecutionDateTime__c,EM_SSBBenchmarkID__c,EM_SSBEntityCode__c,EM_Status__c,EM_Trust__c,Id,Name,OwnerId,RecordTypeId
        am1.EM_AggregateRuleDescription__c = 'EM_Trust__c = \'HCGF\'';
        am1.EM_Trust__c = 'HCF';
        am1.EM_AladdinIDRequiredFlag__c = 'No';
        am1.EM_BenchmarkRequiredFlag__c = 'No';
        am1.EM_BoardConsultantAggregate__c = false;
        am1.EM_AggregateEntityCode__c = 'entitytest3';
        am1.EM_AggregateReportingName__c = 'fdhdh';
        am1.EM_EntityTypeDescription__c = 'Governance Aggregate';
       // am1.EM_Execute__c = false;
        am1.EM_IncentiveCompensationAggregate__c = 'No';
        am1.EM_PortfolioRuleDescription__c = 'EM_SsbPortfolioStatus__c = \'Actived\'';
        am1.EM_RuleExecutionDateTime__c = system.now();
        Id prid1 = Schema.SObjectType.EM_AggregateMaster__c.getRecordTypeInfosByName()
            .get('Aggregate Master')
            .getRecordTypeId();
        am1.RecordTypeId = prid1;
        am1.EM_Status__c ='Pending Opening';
        am1.EM_RecordStatus__c='Pending Implementation';
        insert am1;
        
        EM_AggregateMaster__c am2 = new EM_AggregateMaster__c();
        //am.EM_AggregateEntityCode__c,EM_AggregateLevel__c,EM_AggregateNotes__c,EM_AggregateRelationship__c,EM_AggregateReportingName__c,EM_AggregateRuleDescription__c,EM_AggregateRuleEffectiveDate__c,EM_AggregateRule__c,EM_AladdinIDRequiredFlag__c,EM_AladdinID__c,EM_BenchmarkRequiredFlag__c,EM_BoardConsultantAggregate__c,EM_ClosureDate__c,EM_ComponentListStatus__c,EM_EffectiveDate__c,EM_EntityTypeCode__c,EM_EntityTypeDescription__c,EM_FileSubmissionDate__c,EM_ICAStatus__c,EM_IncentiveCompensationAggregate__c,EM_InceptionDate__c,EM_PerformanceEndDate__c,EM_PerformanceNotes__c,EM_PerformanceStartDate__c,EM_PortfolioRuleDescription__c,EM_PrimaryBenchmarkEffectiveDate__c,EM_PrimaryBenchmarkNumber__c,EM_PrimaryBenchmarkReturnType__c,EM_PrimaryOfficialBenchmarkID__c,EM_PrimaryOfficialBenchmarkName__c,EM_RecordStatus__c,EM_RuleExecutionDateTime__c,EM_SSBBenchmarkID__c,EM_SSBEntityCode__c,EM_Status__c,EM_Trust__c,Id,Name,OwnerId,RecordTypeId
        am2.EM_AggregateRuleDescription__c = 'EM_Trust__c = \'HCF\'';
        //am.EM_AggregateRule__c = 'Portfolio = EM_SsbPortfolioStatus__c = \'Actived\' &  Aggregate = EM_Trust__c = \'HCGF\'';
        am2.EM_AladdinIDRequiredFlag__c = 'No';
        am2.EM_BenchmarkRequiredFlag__c = 'No';
        am2.EM_BoardConsultantAggregate__c = false;
        am2.EM_AggregateEntityCode__c = 'entitytest22';
        am2.EM_AggregateReportingName__c = 'fdhhdh';
        am2.EM_EntityTypeDescription__c = 'Governance Aggregate';
        //am2.EM_Execute__c = false;
        am2.EM_IncentiveCompensationAggregate__c = 'No';
        am2.EM_PortfolioRuleDescription__c = 'EM_SsbPortfolioStatus__c = \'Active\'';
        am2.EM_RuleExecutionDateTime__c = system.now();
        am2.EM_Status__c ='Pending Opening';
        am2.EM_RecordStatus__c='Pending Implementation';
        am2.RecordTypeId = prid;
        
        insert am2;
        
        EM_PortfolioMaster__c pm = new EM_PortfolioMaster__c();
        pm.EM_Ab2833RequiredReporting__c = false;
        //pm.EM_FileSubmissionCheckbox__c = true;
        pm.EM_InternalUseOnly__c = false;
        pm.EM_PolicyProgram__c = 'LLER';
        pm.EM_SsbEntityCode__c = 'Qwe';
        pm.EM_SsbPortfolioStatus__c = 'Active';
        pm.EM_Status__c = 'Active';
        pm.EM_Trust__c = 'PERF';
        insert pm;
        
        EM_PortfolioMaster__c pm1 = new EM_PortfolioMaster__c();
        pm1.EM_Ab2833RequiredReporting__c = false;
        //pm.EM_FileSubmissionCheckbox__c = true;
        pm1.EM_InternalUseOnly__c = false;
        pm1.EM_PolicyProgram__c = 'LLER';
        pm1.EM_SsbEntityCode__c = 'Rty';
        pm1.EM_SsbPortfolioStatus__c = 'Active';
        pm1.EM_Status__c = 'Active';
        pm1.EM_Trust__c = 'PERF';
        insert pm1;
        
        EM_PortfolioMaster__c pm2 = new EM_PortfolioMaster__c();
        pm2.EM_Ab2833RequiredReporting__c = false;
        //pm.EM_FileSubmissionCheckbox__c = true;
        pm2.EM_InternalUseOnly__c = false;
        pm2.EM_PolicyProgram__c = 'LLER';
        pm2.EM_SsbEntityCode__c = 'Asd';
        pm2.EM_SsbPortfolioStatus__c = 'Active';
        pm2.EM_Status__c = 'Active';
        pm2.EM_Trust__c = 'PERF';
        insert pm2;
        
        EM_AggregateComponent__c ac = new EM_AggregateComponent__c();
        Id portrid = Schema.SObjectType.EM_AggregateComponent__c.getRecordTypeInfosByName().get('Portfolio').getRecordTypeId();
        ac.EM_AggregateMasterParent__c = am.id;
        ac.RecordtypeId = portrid;
        ac.EM_PortfolioMaster__c = pm1.id; 
        ac.EM_ComponentMembershipStatus__c = 'Active'; 
        ac.EM_ComponentStartDate__c = Date.today();
        ac.EM_ComponentEndDate__c = Date.today().AddDays(1);
        insert ac;
        
        EM_AggregateComponent__c ac1 = new EM_AggregateComponent__c();
        Id arid = Schema.SObjectType.EM_AggregateComponent__c.getRecordTypeInfosByName().get('Aggregate').getRecordTypeId();
        ac1.EM_AggregateMasterParent__c = am.id;
        ac1.RecordtypeId = arid;
        ac1.EM_AggregateMaster__c = am2.id; 
        ac1.EM_ComponentMembershipStatus__c = 'Active'; 
        ac1.EM_ComponentStartDate__c = Date.today(); 
        ac1.EM_ComponentEndDate__c = Date.today().AddDays(1); 
        insert ac1;
        
        EM_AggregateComponent__c ac2 = new EM_AggregateComponent__c();
        ac2.EM_AggregateMasterParent__c = am.id;
        ac2.RecordtypeId = portrid;
        ac2.EM_PortfolioMaster__c = pm2.id; 
        ac2.EM_ComponentMembershipStatus__c = 'Active'; 
        ac2.EM_ComponentStartDate__c = Date.today();
        ac2.EM_ComponentEndDate__c = Date.today().AddDays(1);
        insert ac2;
        
        EM_AggregateMasterOperations.FlowInputs fInput = new EM_AggregateMasterOperations.FlowInputs();
        fInput.parentObjectId = am.Id;
        fInput.portfolioIds = new List<Id>{pm.Id,pm1.Id};
        fInput.aggregateIds = new List<Id>{am1.Id,am2.Id};
        fInput.portfolioRuleDescription = 'EM_SsbPortfolioStatus__c = \'Actived\'';
        fInput.aggregateRuleDescription = 'EM_Trust__c = \'HCF\'';
        
        List<EM_AggregateMasterOperations.FlowOutputs> op = EM_AggregateMasterOperations.componentOperation(new List<EM_AggregateMasterOperations.FlowInputs> {fInput});
    }
    
}