public class EM_AggregateComponentController {
/********
* Class Name:EMAggregateComponentController
* Functionality: This class handles operations related to EM_AggregateComponent__c.
*********/
    /*Holds the uncommon aggregate component records for Aggregate Master*/
    public static Map<Id,EM_AggregateComponent__c> aggregateMasterIdMap = new Map<Id,EM_AggregateComponent__c>();
    /*Holds the uncommon aggregate component records for Portfolio Master*/
    public static Map<Id,EM_AggregateComponent__c> portfolioMasterIdMap = new Map<Id,EM_AggregateComponent__c>();
  
    /*
    * It provides already existed Aggregate components
    * In form of map of AggregareMasterId with Aggregate component record & portfolioMasterId with Aggregate component record
    */
    
    public List<EM_AggregateComponent__c> getAggregateComponent(EM_AggregateMaster__c am,string operation){
        List<EM_AggregateComponent__c> existingACList = [select Id,EM_AggregateMaster__c,EM_PortfolioMaster__c,EM_ComponentMembershipStatus__c,EM_AggregateMasterParent__r.EM_ComponentListStatus__c from EM_AggregateComponent__c where EM_AggregateMasterParent__c=:am.Id];
        List<EM_AggregateComponent__c> finalACList = new List<EM_AggregateComponent__c>();
        for(EM_AggregateComponent__c aComponent : existingACList){
            if(aComponent.EM_AggregateMaster__c != NULL && (operation=='Aggregate'|| operation==NULL)){
                EM_AggregateComponentController.aggregateMasterIdMap.put(aComponent.EM_AggregateMaster__c,aComponent);
                finalACList.add(aComponent);
            }
            if(aComponent.EM_PortfolioMaster__c != NULL && (operation=='Portfolio' || operation==NULL)){
                EM_AggregateComponentController.portfolioMasterIdMap.put(aComponent.EM_PortfolioMaster__c,aComponent);
                finalACList.add(aComponent);
            }
            
        }
         system.debug('EM_AggregateComponentController.aggregateMasterIdList............'+EM_AggregateComponentController.aggregateMasterIdMap);
         system.debug('EM_AggregateComponentController.portfolioMasterIdList............'+EM_AggregateComponentController.portfolioMasterIdMap);
         return finalACList;
    }
    
    /*
    * This method take care the operations required on uncommon Aggregate Component records
    */
    public void updateAggregateComponent(EM_AggregateMaster__c am,List<EM_AggregateComponent__c> existingACList){
        if(existingACList.size()>0){
                system.debug('Updating existingACList.......'+existingACList);
                for(EM_AggregateComponent__c aComponent : existingACList){
                    aComponent.EM_ComponentMembershipStatus__c = 'Inactive';
                    aComponent.EM_ComponentEndDate__c = Date.today();
                }
                update existingACList;         
        }
    }
    
    /*
    * This method update the parent aggregate master record from where the proecess started
    * The fields has been updated with appropriate values
    */
    public static EM_AggregateMaster__c updateMasterParent(EM_AggregateMasterOperations.Flowinputs req){
        EM_AggregateMaster__c am = [select Id,EM_AggregateRuleDescription__c,EM_PortfolioRuleDescription__c,EM_RecordStatus__c,EM_Status__c,EM_ComponentListStatus__c from EM_AggregateMaster__c where Id=:req.parentObjectId limit 1];
        //am.EM_Execute__c   = false;
        am.EM_RuleExecutionDateTime__c = datetime.now();
        am.EM_ComponentListStatus__c = 'Draft';
        am.EM_Status__c = 'Pending Opening';
        am.EM_AggregateRuleDescription__c= req.aggregateRuleDescription;
        am.EM_PortfolioRuleDescription__c= req.portfolioRuleDescription;
        am.EM_RecordStatus__c ='Pending Implementation';
        update am;
        return am;
    }   
}