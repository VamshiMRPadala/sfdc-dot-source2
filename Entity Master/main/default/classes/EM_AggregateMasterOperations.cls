public class EM_AggregateMasterOperations{
/********
* Class Name:EM_AggregateMasterOperations
* Functionality: This class would be invoked by By visual workflow which passes the ParentRecordId, Aggregate Record Ids and portfolio Id's to
   insert/update aggregate components and update Aggregate master record 
********/

/* Capturing all the flow Inputs */  
    public class FlowInputs{
        /* Holds the parent Aggregate Master record Id, for which the process has been started*/
        @InvocableVariable
        public string parentObjectId;
        
        /* Holds the list of Portfolio Master Ids sent queried in visual flow*/
        @InvocableVariable
        public List<string> portfolioIds = new list<string>();
        
        /* Holds the list of Aggregate Master Ids sent queried in visual flow*/
        @InvocableVariable
        public List<string> aggregateIds = new list<string>();
        
        /* Holds the portfolio query string from visual flow*/
        @InvocableVariable
        public String portfolioRuleDescription;
        
        /* Holds the aggregate query string from visual flow*/
        @InvocableVariable
        public String aggregateRuleDescription;

        
    }
    /*
    * Output details: goes from apex to flow (if needed)
    */ 
    public class FlowOutputs{
        
        @InvocableVariable
        public String resultVar;

    }
    
   
   /*
    * Validate the flow Inputs
    * Call appropriate methods for Aggregate Master changes or Portfolio Master Changes
    * Insert new Aggregate Component records
    * Update uncommon Aggregate Component records
    */  
    @invocablemethod  
    public static List<FlowOutputs> componentOperation (List<FlowInputs> requests) {
        List<EM_AggregateComponent__c> insertionACList = new List<EM_AggregateComponent__c>();
        List<EM_AggregateComponent__c> modifyingACList = new List<EM_AggregateComponent__c>();
        system.debug('***** Value from Flow request 1 - '+ requests);
        if(requests.size() == 1){
            EM_AggregateMaster__c am = EM_AggregateComponentController.updateMasterParent(requests.get(0));
            EM_AggregateComponentController ecc = new EM_AggregateComponentController();
            system.debug('***** aggregate ids- '+ requests.get(0).aggregateIds);
          //  system.debug('***** aggregate ids- '+ requests.get(0).aggregateIds.size());
            if(requests.get(0).aggregateIds != null ){
                ecc.getAggregateComponent(am,'Aggregate');
                insertionACList.addAll(aggregateChanges(requests.get(0).aggregateIds, am));
            }
            if(requests.get(0).portfolioIds!= null ){
                ecc.getAggregateComponent(am,'Portfolio'); 
                insertionACList.addAll(portfolioChanges(requests.get(0).portfolioIds, am));
            }  
            system.debug('***** inserting records- '+ insertionACList);
            modifyingACList.addAll(EM_AggregateComponentController.portfolioMasterIdMap.values());
            modifyingACList.addAll(EM_AggregateComponentController.aggregateMasterIdMap.values());
            system.debug('***** modifyingACList records- '+ modifyingACList);
            ecc.updateAggregateComponent(am,modifyingACList);
            try{
            insert insertionACList;  
            }Catch(exception e){
            }
            
            
               
        }
        //Place holder for any exceptions/Success messages to send to the flow ouput
         List<FlowOutputs> resultsList = new List<FlowOutputs>();           
            FlowOutputs result = new FlowOutputs();
            result.resultVar = 'Success! Please click OK to confirm the creation of the Aggregate Component records based on your rule entry.';
            resultsList.add(result);
            return resultsList;   
    }
    
    /*
    * Prepare the list of Aggregate Components with changes related to Aggregate Master rrecord Ids
    */
    public static List<EM_AggregateComponent__c> aggregateChanges(List<String> aggregateIds, EM_AggregateMaster__c am){
       List<EM_AggregateComponent__c> newAc = new List<EM_AggregateComponent__c>();
       system.debug('aggregateIds......'+aggregateIds);
       system.debug('EM_AggregateComponentController.aggregateMasterIdMap......'+EM_AggregateComponentController.aggregateMasterIdMap);
       for(EM_AggregateMaster__c m : [select Id from EM_AggregateMaster__c where ID IN : aggregateIds]){
            EM_AggregateComponent__c ac1 = new EM_AggregateComponent__c();
            Id arid = Schema.SObjectType.EM_AggregateComponent__c.getRecordTypeInfosByName().get('Aggregate').getRecordTypeId();
            ac1.EM_AggregateMasterParent__c = am.id;
            ac1.RecordtypeId = arid;
            ac1.EM_AggregateMaster__c = m.id; 
            ac1.EM_ComponentMembershipStatus__c = 'Active'; 
            ac1.EM_ComponentStartDate__c = Date.today();  
            if(EM_AggregateComponentController.aggregateMasterIdMap.containsKey(m.Id) && EM_AggregateComponentController.aggregateMasterIdMap.get(m.Id).EM_ComponentMembershipStatus__c=='Active'){
                EM_AggregateComponentController.aggregateMasterIdMap.remove(m.Id);
           }else{
                newAc.add(ac1);
            }
       }
       return newAc;
    }
    
    /*
    * Prepare the list of Aggregate Components with changes related to Portfolio Masrter record Ids
    */
    public static List<EM_AggregateComponent__c> portfolioChanges(List<String> portfolioIds, EM_AggregateMaster__c am){
       List<EM_AggregateComponent__c> newAc = new List<EM_AggregateComponent__c>();
       system.debug('portfolioIds......'+portfolioIds);
       system.debug('EM_AggregateComponentController.portfolioMasterIdMap......'+EM_AggregateComponentController.portfolioMasterIdMap);
       for(EM_PortfolioMaster__c p : [select Id from EM_PortfolioMaster__c where ID IN : portfolioIds]){
            EM_AggregateComponent__c ac = new EM_AggregateComponent__c();
            Id prid = Schema.SObjectType.EM_AggregateComponent__c.getRecordTypeInfosByName().get('Portfolio').getRecordTypeId();
            ac.EM_AggregateMasterParent__c = am.id;
            ac.RecordtypeId = prid;
            ac.EM_PortfolioMaster__c = p.id; 
            ac.EM_ComponentMembershipStatus__c = 'Active'; 
            ac.EM_ComponentStartDate__c = Date.today();
            if(EM_AggregateComponentController.portfolioMasterIdMap.containsKey(p.Id) && EM_AggregateComponentController.portfolioMasterIdMap.get(p.Id).EM_ComponentMembershipStatus__c=='Active'){
                EM_AggregateComponentController.portfolioMasterIdMap.remove(p.Id);
           }else{
                 newAc.add(ac);
            }
       }
       return newAc;
    }
   
}