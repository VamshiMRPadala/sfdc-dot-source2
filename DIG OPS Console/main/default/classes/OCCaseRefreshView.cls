public class OCCaseRefreshView{
   @AuraEnabled 
    public static boolean updateCaseViewedByUsers(Id recId){
       system.debug('get called from Lightning');
        Case cs = [select Id,OC_IsOpened__c from case where Id =:recId ];
        if(cs.OC_IsOpened__c==NULL || cs.OC_IsOpened__c !='Viewed'){
            if(cs.OC_IsOpened__c==NULL)
                cs.OC_IsOpened__c = 'Viewed';
            else
                cs.OC_IsOpened__c = cs.OC_IsOpened__c +userInfo.getUserId()+';';
           // else if(cs.CaseViewedByUsers__c.length()<235)
             //   cs.CaseViewedByUsers__c = cs.CaseViewedByUsers__c +userInfo.getUserId()+';';
          update cs;
            cs = [select Id,OC_IsOpened__c,OC_IsViewed__c from case where Id =:recId ];
            return true;
        }
        return false;
    }
}