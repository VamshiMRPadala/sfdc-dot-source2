public class AddCaseTeamMemberController {
	@AuraEnabled
    public static void saveRecord(string caseId,string memberId,string roleId){
        CaseTeamMember ctm = new CaseTeamMember();
        ctm.ParentId = caseId;
     //   ctm.MemberId = memberId;
        ctm.TeamRoleId = roleId;
        insert ctm;
    }
    
    @AuraEnabled
    public static List<CaseTeamRole> getTeamRoles(){
    	return [SELECT Id, Name FROM CaseTeamRole];
    }
}