@isTest
private class AddCaseTeamMemberController_Test {
    @testSetup static void methodName() {
        CaseTeamRole teamRole = new CaseTeamRole();
        teamRole.name = 'Collaborator2';
        teamRole.AccessLevel = 'Edit';
        insert teamRole;
    }
    @isTest 
    private static void AddTeamMemberTest() {
        // code_block
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Krish000@gurram.com',
            Username = 'Krish000@gurram.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            // UserRoleId = r.Id
        );
        insert u;
        
        List<CaseTeamRole> roleList = AddCaseTeamMemberController.getTeamRoles();
        case c = new case();
        c.subject = 'RE: CTS/GTP re-books';
        insert c;
        AddCaseTeamMemberController.saveRecord(c.id+'', u.Id+'', roleList[0].Id+'');
    }
    
    @isTest 
    private static void ReusableCustomLookUpController_CC_Test(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Krish000@gurram.com',
            Username = 'Krish000@gurram.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            // UserRoleId = r.Id
        );
        insert u;
        ReusableCustomLookUpController_CC.fetchLookUpValues('last','User');
    }
}