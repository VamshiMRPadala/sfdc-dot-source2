public class ReusableCustomLookUpController_CC {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        String sQuery;
        List < sObject > returnList = new List < sObject > ();
        Map<String, Schema.SObjectField> schemaFieldMap;
        System.debug('Objectname in fetchLookUpValues.....'+objectName);
        String fields ='';
        if(ObjectName=='Contact'){
            schemaFieldMap = Schema.SObjectType.Contact.fields.getMap();
        }else if(ObjectName=='User'){
            schemaFieldMap = Schema.SObjectType.User.fields.getMap();
        } 
        for (String fieldName: schemaFieldMap.keySet()) {
            if(fieldName!='Id')
                fields = fields+','+fieldName;
        }
        String searchKey = '%' + searchKeyWord + '%';
        //sQuery =  'select Id'+fields+ ' from ' + ObjectName + ' where Contact_EmployeeId__c LIKE: searchKey';
        sQuery =  'select Id'+fields+ ' from ' + ObjectName + ' where IsActive=true and Name LIKE \''+searchKey+'\' limit 9';
        system.debug('Query.....'+sQuery);
        if(sQuery!=NULL){
            List < sObject > lstOfRecords = Database.query(sQuery);
            for (sObject obj: lstOfRecords) {
                returnList.add(obj);
            }
        }
        return returnList;
    }
}