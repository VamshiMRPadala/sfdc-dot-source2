public class CloseCaseController {
	@AuraEnabled
    public static void closeCase(string caseId){
        Case c = new Case();
        c.Id = caseId;
        c.status = 'Closed';
        c.OwnerId = UserInfo.getUserId();
		update c;
    }
}