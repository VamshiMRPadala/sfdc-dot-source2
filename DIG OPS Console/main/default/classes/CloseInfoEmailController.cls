public class CloseInfoEmailController {
	@AuraEnabled
    public static void closeCaseInfo(string caseId){
        Case c = new Case();
        c.Id = caseId;
        c.status = 'Closed - Info Email';
		update c;
    }
}