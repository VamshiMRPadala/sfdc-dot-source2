public class OCAdjustDuplicateEmailCase implements Queueable {
    public List<Case> newCaseList = new List<Case>();
    public Map<String,case> subjectVsPrevCaseId = new Map<String,case>(); 
    public Map<Id,set<string>> prevCaseIdVsccAddr = new Map<Id,set<string>>();
    public Map<Id,set<string>> prevCaseIdVsToAddr = new Map<Id,set<string>>();
    public void execute(QueueableContext context) {
        List<EmailMessage> emailnew= new List<EmailMessage>();
        List<Case> updateCaseList = new List<Case>();
         List <Attachment> atmnt = new  List <Attachment>();
        List <Case> updateCaseList1= new  List<Case>();
        Set<Id> updateParentCaseIdSet = new set<Id>();
        System.debug('newCaseList.......'+newCaseList);
        for( Case c : [select Id, OwnerId, Subject,Status,(SELECT id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody FROM EmailMessages ) from Case where Id IN :subjectVsPrevCaseId.values()]){
            if(String.isNotBlank(c.EmailMessages.get(0).CcAddress)){
                List<String> ccAddresses = c.EmailMessages.get(0).CcAddress.split(';');
                Set<String> ccAddressSet = new Set<String>(); 
                ccAddressSet.addAll(ccAddresses);
                prevCaseIdVsccAddr.put(c.Id,ccAddressSet);
            }
            List<String> toAddresses = c.EmailMessages.get(0).ToAddress.split(';');
            Set<String> toAddressSet = new Set<String>(); 
            toAddressSet.addAll(toAddresses);
            prevCaseIdVsToAddr.put(c.Id,toAddressSet);
        }
        for( Case c : [select Id, OwnerId, Subject,(SELECT id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody FROM EmailMessages ) from Case where Id IN :newCaseList] ){
            System.debug('c.EmailMessages.......'+c.EmailMessages);
            Set<id> eid = new set<id>();
            for(Emailmessage em1: c.EmailMessages ){
               // Set<String> ccAddressSet = new Set<String>();
              //  if(String.isNotBlank(em1.CcAddress)){
              //      ccAddressSet.addAll(em1.CcAddress.split(';'));
               // }
               eid.add(em1.id);
                //System.debug('new ccAddresses.......'+ccAddressSet);
                string str = em1.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
               // if(prevCaseIdVsccAddr!=NULL && prevCaseIdVsccAddr.containsKey(subjectVsPrevCaseId.get(str).Id))
                 //   System.debug('old ccAddresses.......'+prevCaseIdVsccAddr.get(subjectVsPrevCaseId.get(str).Id));
              //  if(compareCC(ccAddressSet,subjectVsPrevCaseId.get(str).Id)){
                  //  System.debug('ccAddresses matches.......');
                    
                   // Set<String> toAddressSet = new Set<String>();
                    //if(String.isNotBlank(em1.ToAddress)){
                   //     toAddressSet.addAll(em1.ToAddress.split(';'));
                  //  }
                  // System.debug('new ToAddresses.......'+ToAddressSet);
                  //  System.debug('old toAddresses.......'+prevCaseIdVsToAddr.get(subjectVsPrevCaseId.get(str).Id));
                    //if(compareTo(toAddressSet,subjectVsPrevCaseId.get(str).Id)){
                        System.debug('ToAddresses matches.......');
                        if( subjectVsPrevCaseId.containsKey(str)){
                            System.debug('toAddresses and subject matches.......');
                            EmailMessage em2 = new EmailMessage();
                            em2.FromName = em1.fromName;
                            em2.fromaddress = em1.fromaddress;
                            em2.ToAddress = em1.toAddress;
                            em2.CcAddress = em1.CcAddress;
                            em2.Subject = em1.subject;
                            em2.TextBody = em1.TextBody;
                            em2.HtmlBody = em1.htmlBody;
                            em2.ParentId = subjectVsPrevCaseId.get(str).Id ; 
                            emailnew.add(em2);
                            
                           
                        
                            Case cs = new Case();
                            cs.Id = c.Id ; 
                            cs.status = 'Auto-Merged';
                           // cs.OwnerId =subjectVsPrevCaseId.get(str).OwnerId;
                            cs.ParentId=subjectVsPrevCaseId.get(str).Id;
                            updateCaseList.add(cs);
                            
                             if(!updateParentCaseIdSet.contains(subjectVsPrevCaseId.get(str).Id)){
                                Case prevCase = subjectVsPrevCaseId.get(str);
                                if(prevCase.status=='Closed' || (prevCase.status=='Closed - Info Email' && string.valueOf(prevCase.OwnerId).startsWith('005'))){
                                    updateParentCaseIdSet.add(prevCase.Id);
                                    prevCase.status = 'In Progress';
                                    updateCaseList.add(prevCase);
                                }  
                                }                                                
                        }
                    }
              //  }
            //}
        }
      
       
          system.debug('emailnew.size().............'+emailnew.size());
        if(emailnew.size()>0){
            system.debug('emailnew.............'+emailnew);
            insert emailnew;    
        }
        
        
        /*  for( Case c : [select Id, OwnerId, Subject,status,(SELECT id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody FROM EmailMessages ) from Case where Id IN :subjectVsPrevCaseId.values()] ){
        if(c.status == 'Closed'){
        c.status = 'In Progress';
        updateCaseList1.add(c);
        }
        }
          if(updateCaseList1.size()>0){
            update updateCaseList1;
        }    */ 
        
        if(updateCaseList.size()>0){
            update updateCaseList;
        }        
    }
    
  /*  public boolean compareCC(set<String> ccAddressesSet,Id caseId){
        if(prevCaseIdVsccAddr ==NULL){
            return true;
        }else if(!prevCaseIdVsccAddr.containsKey(caseId)){
              return true; 
        }else if(ccAddressesSet.size()==0 && caseId==NULL){
            return true;
        }
        for(string ccaddr: prevCaseIdVsccAddr.get(caseId)){
            if(!ccAddressesSet.contains(ccaddr)){
                return false;
            }
        }
        return true;
    }*/
    
  /*  public boolean compareTo(set<String> toAddressesSet,Id caseId){
        if(!prevCaseIdVsToAddr.containsKey(caseId) && toAddressesSet.size()>0){
            return false;
        }
        for(string ccaddr: prevCaseIdVsToAddr.get(caseId)){
            if(!toAddressesSet.contains(ccaddr)){
                return false;
            }
        }
        return true;
    }*/
    
}