@isTest
public class OCDuplicateCaseFinderUtilityTest {
    static testmethod void AutoRelateEmailMessageTriggerTest() {
        
        Test.startTest();
        
        // Create Case
        //Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business').getRecordTypeId();
        Case cs = new Case(
            subject='ABC',
            recordtypeId = '0123l000001ha8WAAQ'
        );
        insert cs;
        
        // Create a ContentVersion
        ContentVersion content_ver = new ContentVersion();
        content_ver.Title = 'case Doc';
        content_ver.ContentUrl= 'test.com';
        content_ver.Description = 'Testing';
        insert content_ver;
        
        // Insert email
        EmailMessage mEmail = new EmailMessage(
            Subject = 'ABC',
            RelatedToId = cs.Id,
            ToAddress='nik@pm.com',
            CcAddress='pm@nik.com'
        );
        insert mEmail;
        system.debug('mEmail...........'+mEmail.Id);
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: content_ver.Id];
        system.debug('contentdocument........'+cv.ContentDocumentId);
        // Create Content Document Link
        ContentDocumentLink document_link = new ContentDocumentLink(
            ContentDocumentId = cv.ContentDocumentId,
            LinkedEntityId = mEmail.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert document_link;
        
        Case cs1 = new Case(
            subject='ABC',
             recordtypeId = '0123l000001ha8WAAQ'
        );
        insert cs1;
        
        // Create a ContentVersion
        ContentVersion content_ver1 = new ContentVersion();
        content_ver1.Title = 'case Doc';
        content_ver1.ContentUrl= 'test.com';
        content_ver1.Description = 'Testing';
        insert content_ver1;
        
        // Insert email
        EmailMessage mEmail1 = new EmailMessage(
            Subject = 'ABC',
            RelatedToId = cs1.Id,
            ToAddress='nik@pm.com',
            CcAddress='pm@nik.com'
        );
        insert mEmail1;
        system.debug('mEmail...........'+mEmail1.Id);
        ContentVersion cv1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: content_ver1.Id];
        system.debug('contentdocument1........'+cv1.ContentDocumentId);
        // Create Content Document Link
        ContentDocumentLink document_link1 = new ContentDocumentLink(
            ContentDocumentId = cv1.ContentDocumentId,
            LinkedEntityId = mEmail1.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert document_link1;
        
        Test.stopTest();
    }
    
    static testmethod void AutoRelateEmailMessageTriggerDigTest() {
        
        Test.startTest();
        
        // Create Case
        //Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business').getRecordTypeId();
        Case cs = new Case(
            subject='ABC',
            recordtypeId = '0123l000001ha8VAAQ'
        );
        insert cs;
        
        // Create a ContentVersion
        ContentVersion content_ver = new ContentVersion();
        content_ver.Title = 'case Doc';
        content_ver.ContentUrl= 'test.com';
        content_ver.Description = 'Testing';
        insert content_ver;
        
        // Insert email
        EmailMessage mEmail = new EmailMessage(
            Subject = 'ABC',
            RelatedToId = cs.Id,
            ToAddress='nik@pm.com',
            CcAddress='pm@nik.com'
        );
        insert mEmail;
        system.debug('mEmail...........'+mEmail.Id);
        ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: content_ver.Id];
        system.debug('contentdocument........'+cv.ContentDocumentId);
        // Create Content Document Link
        ContentDocumentLink document_link = new ContentDocumentLink(
            ContentDocumentId = cv.ContentDocumentId,
            LinkedEntityId = mEmail.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert document_link;
        
        Case cs1 = new Case(
            subject='ABC',
             recordtypeId = '0123l000001ha8VAAQ'
        );
        insert cs1;
        
        // Create a ContentVersion
        ContentVersion content_ver1 = new ContentVersion();
        content_ver1.Title = 'case Doc';
        content_ver1.ContentUrl= 'test.com';
        content_ver1.Description = 'Testing';
        insert content_ver1;
        
        // Insert email
        EmailMessage mEmail1 = new EmailMessage(
            Subject = 'ABC',
            RelatedToId = cs1.Id,
            ToAddress='nik@pm.com',
            CcAddress='pm@nik.com'
        );
        insert mEmail1;
        system.debug('mEmail...........'+mEmail1.Id);
        ContentVersion cv1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: content_ver1.Id];
        system.debug('contentdocument1........'+cv1.ContentDocumentId);
        // Create Content Document Link
        ContentDocumentLink document_link1 = new ContentDocumentLink(
            ContentDocumentId = cv1.ContentDocumentId,
            LinkedEntityId = mEmail1.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert document_link1;
        
        Test.stopTest();
    }
    
    static testmethod void fundRelatedContentTest() {
        // Setup test data
        String roleIds = Label.Assets_Role;
        List<string> roleIdList = roleIds.split(',');
        // Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=roleIdList.get(0),
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        
        System.runAs(u) {
            // The following code runs as user 'u'
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            // Create a ContentVersion
            ContentVersion content_ver = new ContentVersion();
            content_ver.Title = 'Account Doc';
            content_ver.ContentUrl= 'test.com';
            content_ver.Description = 'Testing';
            insert content_ver;
            
            Account acc= new Account();
            acc.name = 'Test Account';
            insert acc; 
            
            ContentVersion cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: content_ver.Id];
            
            ContentDocumentLink document_link = new ContentDocumentLink(
                ContentDocumentId = cv.ContentDocumentId,
                LinkedEntityId = acc.Id,
                ShareType = 'V',
                Visibility = 'AllUsers'
            );
            insert document_link;
        }
    }
}  