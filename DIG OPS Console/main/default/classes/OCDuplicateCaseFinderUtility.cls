public class OCDuplicateCaseFinderUtility implements Queueable {
    public List<Case> newCases;
    public Map<String,EmailMessage> subjectVsEmail;
    public Map<Id,List<ContentDocumentLink>> emailIdVsCDL;
    
    public OCDuplicateCaseFinderUtility(List<Case> caseList,Map<String,EmailMessage> subjectVsEmail,Map<Id,List<ContentDocumentLink>> emailIdVsCDL){
        this.newCases = caseList;
        this.subjectVsEmail = subjectVsEmail;
        this.emailIdVsCDL = emailIdVsCDL;
    }
    public void execute(QueueableContext context) {
        Map<String,Case> subjectVsCase = new  Map<String,Case>();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        for( Case newCase : newCases ){
            string str = newCase.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
            subjectVsCase.put(str,newCase);
        }
        system.debug('subjectVsCase.......'+subjectVsCase);
        Map<String,case> subjectVsPrevCaseId = new  Map<String,case>();
        for (Case oldCase :[select Id, OwnerId, Subject,recordtypeId,(SELECT id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody FROM EmailMessages order by createdDate desc) from Case where Id Not IN: newCases and OCCaseSubject__c IN :subjectVsCase.keySet() and createddate=TODAY and status<>'Auto-Merged']){
            string str = oldCase.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
            system.debug('str.......'+str);
            system.debug('subjectVsEmail.......'+subjectVsEmail);
            if(subjectVsEmail.containsKey(str)){
                EmailMessage newEmailMessage = subjectVsEmail.get(str);
                if(oldCase.recordtypeId == subjectVsCase.get(str).recordtypeId){
                    for(EmailMessage oldEmailMessage : oldCase.EmailMessages){
                        //if(oldEmailMessage.CcAddress == newEmailMessage.CcAddress && oldEmailMessage.ToAddress == newEmailMessage.ToAddress){
                            for(ContentDocumentLink cdl : emailIdVsCDL.get(newEmailMessage.Id)){
                            system.debug('InsideCDL.......');
                                ContentDocumentLink newCdl = cdl.clone();
                                newCdl.LinkedEntityId = oldEmailMessage.ParentId;
                                cdlList.add(newCdl);
                            }
                            break;
                        //}
                    }
                }
            }
        }
        
        //insert cdl
        insert cdlList;
    }
}