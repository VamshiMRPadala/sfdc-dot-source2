trigger OCDuplicateCaseIdentifier on Case (before insert,after insert) {
    
    if(trigger.isInsert){
        Map<String,Case> subjectVsCase = new  Map<String,Case>();
         Map<String,Case> OGsubjectVsCase = new  Map<String,Case>();
        for( Case c : Trigger.new ){
        if(c.subject !=null && c.subject!=''){
            string str = c.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
            subjectVsCase.put(str,c);
            OGsubjectVsCase.put(c.subject,c);
            }
        }  
        //if(trigger.isBefore){
            //for (Case c :[select Id, OwnerId, Subject from Case where Subject IN :subjectVsCase.keySet() and createddate=TODAY and status<>'Closed']){
            //    subjectVsCase.get(c.Subject).status = 'Closed';
            //}
        //}
        
        if(trigger.isAfter){ 
            Map<String,case> subjectVsPrevCaseId = new  Map<String,case>();
            for (Case c :[select Id, OwnerId, Subject,status,(SELECT id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody FROM EmailMessages ) from Case where Id Not IN: trigger.new and (Subject IN :subjectVsCase.keyset() OR Subject IN :OGsubjectVsCase.keyset())   and createddate=TODAY and status<> 'Auto-Merged']){
                string str = c.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
                subjectVsPrevCaseId.put(str,c);
            }
            
            if(subjectVsPrevCaseId.size()>0){
            OCAdjustDuplicateEmailCase adec = new OCAdjustDuplicateEmailCase();
            System.debug('trigger.new.......'+trigger.new);
            adec.newCaseList=trigger.new;
            adec.subjectVsPrevCaseId = subjectVsPrevCaseId;       
            System.enqueueJob(adec);
            }
        }
    }
}