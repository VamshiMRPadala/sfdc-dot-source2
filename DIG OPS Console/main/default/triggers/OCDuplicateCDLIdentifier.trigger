trigger OCDuplicateCDLIdentifier on ContentDocumentLink (After Insert) {
    if(trigger.isAfter && trigger.isInsert){
        /*Schema.SObjectType token = objId.getSObjectType();
        Schema.DescribeSObjectResult dr = token.getDescribe();
        system.debug('object name......' + dr.getName()); */
        List<ContentDocumentLink> checkDcocumentLinkList = new List<ContentDocumentLink>();
        Map<Id,List<ContentDocumentLink>> emailIdVsCDL = new Map<Id,List<ContentDocumentLink>>();
        List<Id> linkIdList = new List<Id>();
        List<Id> fundDocumentIdList = new List<Id>();
        for(ContentDocumentLink cdl : trigger.new){
            String parentId = cdl.LinkedEntityId+'';
            if(parentId.startswith('02s')){
                checkDcocumentLinkList.add(cdl);
                linkIdList.add(cdl.LinkedEntityId);
                if(emailIdVsCDL.containsKey(cdl.LinkedEntityId)){
                    emailIdVsCDL.get(cdl.LinkedEntityId).add(cdl); 
                }else{
                    emailIdVsCDL.put(cdl.LinkedEntityId,new list<ContentDocumentLink>{cdl});  
                }
                 }else  if(parentId.startsWith('a0L') || parentId.startsWith('001')  ){     
                fundDocumentIdList.add(cdl.contentDocumentId);          
            }
        }
        String roleIds = Label.Assets_Role;
        if(roleIds.contains(UserInfo.getUserRoleId())){
            List<ContentVersion> versionList = [SELECT Id, ContentDocumentId, RecordTypeId, IsLatest, ContentUrl, ContentBodyId, VersionNumber, Title, Description, ReasonForChange, SharingOption, SharingPrivacy, PathOnClient, RatingCount, IsDeleted, ContentModifiedDate, ContentModifiedById, PositiveRatingCount, NegativeRatingCount, FeaturedContentBoost, FeaturedContentDate, OwnerId, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate, SystemModstamp, TagCsv, FileType, PublishStatus, VersionData, ContentSize, FileExtension, FirstPublishLocationId, Origin, ContentLocation, TextPreview, ExternalDocumentInfo1, ExternalDocumentInfo2, ExternalDataSourceId, Checksum, IsMajorVersion, IsAssetEnabled, AssetClas__c, Document_Type__c, OwnerChangeDate__c, TestPC__c, Asset_Class__c FROM ContentVersion
                                                where ContentDocumentId IN : fundDocumentIdList];
            system.debug('versionList........'+versionList);
            for(ContentVersion cv : versionList ){
                cv.SharingOption='R';
                cv.SharingPrivacy='P';
            }
            system.debug('Updated versionList.......'+versionList);
            if(versionList.size()>0){
                update versionList;
             }
            ContentConstant.allowContentChange = false;
        }
        List<EmailMessage> emList = new List<EmailMessage>();
        List<Id> caseIdList = new List<Id>();
        Map<String,EmailMessage> subjectVsEmail = new Map<String,EmailMessage>();
        if(checkDcocumentLinkList.size()>0){
            for(EmailMessage em : [select Id,fromName,fromaddress, ParentId,ToAddress,Subject,CcAddress,MessageDate,TextBody,htmlBody,parent.subject from EmailMessage where Id in :linkIdList]){
                String parentId = em.parentId+'';
                
                if(parentId.startswith('500')){
                    caseIdList.add(em.parentId);
                    emList.add(em);
                    string str = em.parent.subject.replaceAll( '(?:\\[?(?:[Ff][Ww][Dd]?|[Rr][Ee])(?:\\s*[:;-]+\\s*\\]?))+' , '');
                    subjectVsEmail.put(str,em);
                }
            }
        } 
        List<case> caseList = [select Id, OwnerId,recordtypeId, Subject,parentId from case where Id IN: caseIdList];
        OCDuplicateCaseFinderUtility dcf = new OCDuplicateCaseFinderUtility(caseList,subjectVsEmail,emailIdVsCDL);
        system.debug('caseList.......'+caseList);
        system.debug('subjectVsEmail.......'+subjectVsEmail);
        system.debug('emailIdVsCDL.......'+emailIdVsCDL);
        System.enqueueJob(dcf);
    }
}