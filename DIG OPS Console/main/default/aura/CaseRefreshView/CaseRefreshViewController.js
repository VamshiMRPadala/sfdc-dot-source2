({
    handleRecordUpdated: function(component, event, helper) {
        var changeType = event.getParams().changeType;
        //alert('changeType....'+changeType);
        var action = component.get("c.updateCaseViewedByUsers");
        action.setParams({
            "recId": component.get("v.recordId")
        })
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var storeResponse = response.getReturnValue();
                if(storeResponse)
                    //location.reload();
                $A.get('e.force:refreshView').fire();
            }
            else{
                console.log('error message'+response.getError());
            }
            
        });
        $A.enqueueAction(action);
    }
})