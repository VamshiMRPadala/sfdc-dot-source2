({
    saveClick : function(component, event, helper) {
        var action = component.get("c.closeCaseInfo");
        // set param to method  
        action.setParams({
            'caseId': component.get("v.recordId")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },
    cancelClick : function(component, event, helper) {
        component.set("v.isOpen","false");
         $A.get('e.force:refreshView').fire();
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire();
    },
})