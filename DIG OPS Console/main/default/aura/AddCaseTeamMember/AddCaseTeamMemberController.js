({
    doInit : function(component, event, helper) {
        var action = component.get("c.getTeamRoles");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse.length>0){
                     component.set("v.roleList", storeResponse);
                }
            }
            
        });
     	 $A.enqueueAction(action);
    },
    cancelClick : function(component, event, helper) {
        component.set("v.isOpen","false");
         $A.get('e.force:refreshView').fire();
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire();
    },
    saveClick : function(component, event, helper) {
        var action = component.get("c.saveRecord");
        //alert(component.get("v.selectedRoleId"));
        //alert(component.get("v.memberId"));
        //alert(component.get("v.recordId"));
        // set param to method  
        action.setParams({
            'caseId': component.get("v.recordId"),
            'memberId' : component.get("v.memberId"),
            'roleId':component.get("v.selectedRoleId")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                 $A.get('e.force:refreshView').fire();
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
        
        component.set("v.isOpen","false");
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire();
    },
    changeRole : function(component, event, helper) {
        //alert(component.get("v.selectedRoleId"));
        //alert(event.getSource().get("v.value"));
    },
     handleComponentEvent : function(component, event, helper) {
        var selectedRecordtGetFromEvent = event.getParam("recordByEvent");
        component.set("v.memberId",selectedRecordtGetFromEvent.Id);     
     },
    closeModel : function(component, event, helper) {
        component.set("v.isOpen","false");
         $A.get('e.force:refreshView').fire();
    },
})