({
    getData : function(cmp) {
        var action = cmp.get('c.getAllregistrations');
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.reg', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }, 
    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message:'Search returned  no results',
            messageTemplate: 'No Records Found',
            duration:'10',
            key: 'info_alt',
            type: 'error', 
            mode: 'dismissable'
        });
        toastEvent.fire();
    }, 
    getData : function(cmp) { 
        var action = cmp.get('c.getAllregistrations');
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.mydata', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    sortBy: function(component, field,reverse, primer) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.currentList"),
            fieldPath = field.split(/\./),
            fieldValue = this.fieldValue;
      //  var key = primer ?
        //    function(x) {return primer(x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa')} :
         //   function(x) {return x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa'};
       // reverse = !reverse ? 1 : -1;               
       sortAsc = sortField != field || !sortAsc;      
      records.sort(function(a,b){
            var aValue = fieldValue(a , fieldPath),
                bValue = fieldValue(b , fieldPath),
               t1 = aValue == bValue,
               t2 = (!aValue && bValue) || (aValue < bValue);                  
          return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        //return t1 = key(t1), t2 = key(t2), reverse * ((t1 > t2) - (t2 > t1));
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.currentList", records);
        this.renderPage(component);
    },
    fieldValue: function(object, fieldPath) {
        var result = object;
        fieldPath.forEach(function(field) {
            if(result) {
                result = result[field];
            }
        });
        return result;
    },
	renderPage: function(component) {
		var records = component.get("v.currentList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*500, pageNumber*500);
        component.set("v.reg", pageRecords);
	
    },
      
   FilterRecords: function(component) {  
     //data showing in table  
     var currentList = component.get("v.currentList");  
     // all data featched from apex when component loaded  
     var allData = component.get("v.UnfilteredData");  
     //Search tems  
     var searchKey = component.get("v.filter");  
     // check is data is not undefined and its lenght is greater than 0  
     if(currentList!=undefined || currentList.length>0){  
       // filter method create a new array tha pass the test (provided as function)  
       var filtereddata = currentList.filter(word => (!searchKey) || word.LinkedEntityName.toLowerCase().indexOf(searchKey.toLowerCase()) > -1);  
       console.log('** '+filtereddata);  
     }  
     // set new filtered array value to data showing in the table.  
     component.set("v.currentList", filtereddata);  
     // check if searchKey is blank  
     if(searchKey==''){  
       // set unfiltered data to data in the table.  
       component.set("v.currentList",component.get("v.currentList"));  
     }  
   }  
  

})
