({
   myAction : function(component, event, helper) {
        var action1 = component.get("c.getAllregistrations");
        var action2 = component.get("c.getAlldocs");
        action1.setParams({
            assetclass : component.get("v.selectedValue"),
            documenttype : component.get("v.docValue"),
            Title :component.get("v.Titlevalue"),          
            startdate : component.get("v.startdatevalue"),
            enddate :component.get("v.enddatevalue"),
            Obj :component.get("v.Objectvalue"),
             
        });
       action2.setParams({
            assetclass : component.get("v.selectedValue"),
            documenttype : component.get("v.docValue"),
            Title :component.get("v.Titlevalue"),
            Obj :component.get("v.Objectvalue"),
           Linkedname:component.get("v.filter")
        });
        action1.setCallback(this, function(response){
            var name = response.getState();
            if (name === "SUCCESS") {
            component.set("v.currentList", response.getReturnValue());
            } 
            
           var actionf = component.get("v.currentList");
            if($A.util.isEmpty(actionf) || $A.util.isUndefined(actionf)){
            helper.showErrorToast(); } 
        });
        $A.enqueueAction(action1);
        action2.setCallback(this, function(response){
            var name = response.getState();
            if (name === "SUCCESS") {
                component.set("v.reg3", response.getReturnValue());               
            }
            //var obj = component.get("v.Objectvalue");
            //var objr = component.get("v.reg2");
           // var newList = [];          
           //  for(var item of objr){
           //   if(obj === 'Account' && item.Linkedentity.type === 'Account'){
           //  newList.push(item);
           //   }              
          //   newList.push();
           // component.set('v.reg2', newList);                 
           //}                 
        });
       $A.enqueueAction(action2);
    },
    clearall: function(component, event, helper) {
         var Obj = component.set("v.Objectvalue" , "");
        var assetclass = component.set("v.selectedValue" , "");
        var documenttype = component.set("v.docValue","");
         var startdate = component.set("v.startdatevalue" , "");
         var Title = component.set("v.Titlevalue" , "");
         var enddate = component.set("v.enddatevalue" , "");
        var b = component.get('c.doInit');
        $A.enqueueAction(b);
	},
    doInit: function(component, event, helper) {
        var action = component.get("c.getAllregistrations");
        action.setCallback(this, function(result) {
            var records = result.getReturnValue();
            component.set("v.currentList", records);
            component.set("v.UnfilteredData", records);
          //  if(records != null){
            component.set("v.maxPage", Math.floor((records.length+9)/10));
            //}
            helper.sortBy(component, "cnttversion.Title");
        });
        $A.enqueueAction(action);
	},
    sortByFileName: function(component, event, helper) {
        
        helper.sortBy(component, "cnttversion.Title");
    },
    sortByAssetclass: function(component, event, helper) {
        helper.sortBy(component, "cnttversion.Asset_Class__c");
    },
    sortByDescription: function(component, event, helper) {
        helper.sortBy(component, "cnttversion.Description");
    },
   sortByDocumentType: function(component, event, helper) {
        helper.sortBy(component, "cnttversion.Document_Type__c");
    },
    sortByCreatedDate: function(component, event, helper) {
        helper.sortBy(component, "cnttversion.CreatedDate");
    },
    sortByLinkedEntityType: function(component, event, helper) {
        helper.sortBy(component, "LinkedEntityType");
    },
    sortByLinkedEntityName: function(component, event, helper) {
       helper.sortBy(component, "LinkedEntityName");
    },
     sortByLastModifiedByName: function(component, event, helper) {
        helper.sortBy(component, "cnttversion.LastModifiedBy.Name");
    },
   navigateToOppty:function(component){
  // it returns only first value of Id
  var Opprec  = component.get("v.oppty");

  var sObectEvent = $A.get("e.force:navigateToSObject");
    sObectEvent .setParams({
    "recordId": Opprec.Id  ,
    "slideDevName": "detail"
  });
       sObectEvent.fire(); 
   },

    myActiond : function(component, event, helper) {
        var action = component.get("c.getAlldocs");
      /*  action.setParams({
            assetclass : component.get("v.selectedValue"),
            documenttype : component.get("v.docValue"),
            Title :component.get("v.Titlevalue")
        });*/
        action.setCallback(this, function(response){
            var name = response.getState();
            if (name === "SUCCESS") {
                component.set("v.reg1", response.getReturnValue());
            }
        });
     $A.enqueueAction(action);
    },
     myActionf : function(component, event, helper) {
            alert('loadOptions==');
        var action = component.get("c.getAccountName");
          alert('loadOption11s==');
        action.setParams({
            assetclass : component.get("v.selectedValue"),
            documenttype : component.get("v.docValue"),
            Title :component.get("v.Titlevalue")
        });
        action.setCallback(this, function(response){
            var name = response.getState();
            if (name === "SUCCESS") {
                component.set("v.name", response.getReturnValue());
            }
        });
     $A.enqueueAction(action);
    },
    doFilter: function(component, event, helper) {  
     //calling helper  
     helper.FilterRecords(component);  
   },  
    loadOptions: function (cmp, event, helper) {
       // alert('loadOptions==');
        var opts = [
            { value: "Absolute Return Strategies", label: "Absolute Return Strategies" },
            { value: "Global Equity", label: "Global Equity" },
            { value: "Global Fixed Income", label: "Global Fixed Income" },
            { value: "Opportunistic Strategies", label: "Opportunistic Strategies" },
            { value: "Private Equity", label: "Private Equity" },
            { value: "Real Assets", label: "Real Assets" },
            { value: "Trust Level Portfolio Management", label: "Trust Level Portfolio Management" }            
         ];
         var opts2 = [
            { value: "Annual Compliance Certifications", label: "Annual Compliance Certifications" },
          //{ value: "Consultant Reports", label: "Consultant Reports" },
            { value: "Manager Assessments", label: "Manager Assessments" }, 
            { value: "Selection MAT", label: "Selection MAT" },
            { value: "Monitoring MAT", label: "Monitoring MAT" },
            { value: "Manager Meeting Notes", label: "Manager Meeting Notes" },
            { value: "Audit Reports", label: "Audit Reports" },
            { value: "Policy", label: "Policy" },
          //{ value: "Operational Due Diligence Memo", label: "Operational Due Diligence Memo" },
          //{ value: "Operational Due Diligence Questionnaire", label: "Operational Due Diligence Questionnaire" },
            { value: "Other", label: "Other" },
            { value: "Routine Monitoring Questionnaires", label: "Routine Monitoring Questionnaires" },
            { value: "Routine Monitoring Reports", label: "Routine Monitoring Reports" }            
             ];
        var opts3 = [
            { value: "Account", label: "Account" },
            { value: "Fund", label: "Fund" },
            
             ];
        
      cmp.set("v.Assetclassvalues", opts);
      cmp.set("v.Documenttypes", opts2);
      cmp.set("v.Objectvalues", opts3);
      console.dir(cmp.get("v.Documenttypes"));
      cmp.set("v.disablefileUpload", true);
      //cmp.set("v.pickval", true);
    }, 
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   }, 
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
    
})
