({
    Delete : function(component, event, helper) {
       
        var rnamee  = component.get("v.rname");
        //alert(rnamee);
        var ccobje  = component.get("v.ccobj");
        //alert(ccobje);
        
  var Opprec  = component.get("v.recordId");
        
        if((rnamee==ccobje) || (rnamee=='System Administrator' || rnamee=='Investment Manager Engagement Program')){
             component.set("v.isOpen1",true); 
        var action = component.get("c.getAllregistrations");
      
        action.setParams({
            ccid : Opprec,
            
        });
        
         action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               var listviews = response.getReturnValue();
                console.log("Record is deleted2");
                
                //alert('Record Deleted3');
                //window.location="https://calpersinvo--emmeituat.lightning.force.com/lightning/o/CalPERSContact__c/list?filterName=Recent";
            $A.get("e.force:closeQuickAction").fire()
            var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "https://calpersinvo--emmeit.lightning.force.com/lightning/o/CalPERSContact__c/list?filterName=Recent"
        });
        urlEvent.fire();
            
            var toastEvent = $A.get("e.force:showToast");
             toastEvent.setParams({
             "title": "Success!",
             "message": "The records has been Deleted",
              duration:' 5000',
              key: 'info_alt',
              type: 'success',
              mode: 'pester'
 });
 toastEvent.fire();              
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                alert('you dont have access');
                document.location.reload(true);
            }
        }));
          
        $A.enqueueAction(action);
         }
        else{
            
          $A.get('e.force:refreshView').fire();
            
   //component.set("v.isOpen1",false); 
   //component.set("v.isOpen2",true);  
            $A.get("e.force:closeQuickAction").fire();
            var toastEvent = $A.get("e.force:showToast");
             toastEvent.setParams({
             "title": "Warning",
             "message": "you dont have access on the Record.Please contact system administrator.",
              duration:' 5000',
              key: 'info_alt',
              type: 'Info',
              mode: 'pester'
 });
 toastEvent.fire();
        }
    },
    cancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire()
            $A.get('e.force:refreshView').fire();
    },
    loadOptions :function(component, event, helper) {
        var action = component.get("c.ciid");
         action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.rname", response.getReturnValue());
                
            }
             
 }));
          
        $A.enqueueAction(action);
    },
    loadOptions2: function(component, event, helper) {
        var Opprec  = component.get("v.recordId");
        
        var action = component.get("c.cc");
      
        action.setParams({
            ccid : Opprec,
            
        });
        
         action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              component.set("v.ccobj", response.getReturnValue());
            }
 }));
          
        $A.enqueueAction(action);
    },
     closeModel: function(component, event, helper) {
     
      component.set("v.isOpen2", false);
       
   },
    
      })