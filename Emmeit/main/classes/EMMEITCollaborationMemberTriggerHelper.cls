public class EMMEITCollaborationMemberTriggerHelper {
    
 
  @future
    public static void createcgmLinks(set<ID> usIds)
    {
        List<CollaborationGroupMember> cgmlist = new List<CollaborationGroupMember>();
    //querying users based on set id's from UserRole trigger
      List<user> urList =[select id,UserRole.name from User where Id IN :usIds];
    
       set<String> roleNames=new   set<String>();
        Map<String, ID> cGroup=new  Map<String, ID>();
         for(user ur1: urList){
             roleNames.add(ur1.UserRole.name);
         }
        system.debug('==astNames==='+roleNames);
        //Selecting Cgm based from user role name from previous rolename list from users
        for(CollaborationGroup cbg :[select id,Name from CollaborationGroup where Name IN :roleNames])
        {
            cGroup.put(cbg.Name, cbg.ID);
        }
         system.debug('==cGroup==='+cGroup);
      for(User ur1: urList){
          system.debug('===cvl.Asset_Class__c==='+ur1.UserRole);
      if(ur1.UserRole.name !=Null && cGroup.containsKey(ur1.UserRole.name)){
      //insert new cgm member with values from userid and userrole name
         CollaborationGroupMember cgm = new CollaborationGroupMember();
            cgm.MemberId=ur1.Id;
            cgm.CollaborationGroupId= cGroup.get(ur1.UserRole.name);
           // cdl.ShareType = 'C';
            cgmlist.add(cgm);
           //  system.debug('-cdllist-');
                        
        }
           insert cgmlist;
        
    }
 }
 
 @future
    Public static void deletecgmOnChange(set<ID> usIds)
    {
         List<CollaborationGroupMember> cgmlinkstobedeleted = new  List<CollaborationGroupMember>();
    // List<ContentdocumentLink> ContentTobeDeleted= [Select id,ContentDocumentId,LinkedEntityId from contentdocumentLink where ContentDocumentId IN :(ContentDocLinks)];
    //list all the CGM's from USid's from userrole Trigger
      for( CollaborationGroupMember cgm: [Select id,MemberId,CollaborationGroupId from CollaborationGroupMember where MemberId IN :usIds]){
       // system.debug('======'+cdl.sharetype);
       if(cgm.MemberId != null){
          cgmlinkstobedeleted.add(cgm);
          system.debug('===22===');
          }
        } 
    
        //system.debug('===22==='+doclinkstobedeleted);
        if(!cgmlinkstobedeleted.isempty()){
        delete cgmlinkstobedeleted ;
        
          } 
           
    
        List<CollaborationGroupMember> cgmlist = new List<CollaborationGroupMember>();
        //querying users based on set id's from UserRole trigger
      List<user> urList =[select id,UserRole.name from User where Id IN :usIds];
    
       set<String> roleNames=new   set<String>();
        Map<String, ID> cGroup=new  Map<String, ID>();
         for(user ur1: urList){
             roleNames.add(ur1.UserRole.name);
         }
        system.debug('==astNames==='+roleNames);
          //Selecting Cgm based from user role name from previous rolename list from users
        for(CollaborationGroup cbg :[select id,Name from CollaborationGroup where Name IN :roleNames])
        {
            cGroup.put(cbg.Name, cbg.ID);
        }
         system.debug('==cGroup==='+cGroup);
      for(User ur1: urList){
          system.debug('===cvl.Asset_Class__c==='+ur1.UserRole);
      if(ur1.UserRole.name !=Null && cGroup.containsKey(ur1.UserRole.name)){
            //insert new cgm member with values from userid and userrole name
         CollaborationGroupMember cgm = new CollaborationGroupMember();
            cgm.MemberId=ur1.Id;
            cgm.CollaborationGroupId= cGroup.get(ur1.UserRole.name);
           // cdl.ShareType = 'C';
            cgmlist.add(cgm);
           //  system.debug('-cdllist-');
                        
        }
        insert cgmlist;
        
    
 }
    }



}