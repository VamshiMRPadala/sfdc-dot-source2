public class EMMEITEMLSnapshotUpdateHelper {

//FYI
//We were unable to schedule this report using Salesforce reporting Snapshot because of following considerations:
//1.Not able to Map multi-select fields to destination object’s fields.
//2.Not able to schedule it on the first day of every quarter in an year.
//For these reasons that we’ll  be creating an apex class in combination with a cron tab to schedule it on the first of every quarter.

public static void EMLUpdate(){
String S = System.Label.EMMEITContentDocumentTime;
integer T = integer.ValueOf(S);

set<id> docids = new set<id>();
  list<EMLSnapshot__c> Elist = new list<EMLSnapshot__c>();
   list<Account> aclist1 =[SELECT Id, AssetClassString__c, EmergingManager__c, EmergingManagerType__c, TransitionManager__c FROM Account where
                                     (RecordType.DeveloperName = 'EMMEIT'
                                      AND Status__c = 'Active'
                                      AND (NOT ExternalManagerList__c = 'No') 
                                      AND DLRSARS__c = 0 
                                      AND (NOT FundInvestmentStructure__c Like 'Direct Investment') 
                                      AND (NOT FundStrategy__c Like 'Hedge Fund')
                                      AND (NOT FundStrategy__c Like 'Affiliates') 
                                      AND (NOT (EmergingManager__c ='Yes' AND FundStrategy__c Like 'EM Advisor'))
                                      AND (NOT (EmergingManager__c ='Yes' AND DLRSGE__c  >0))) 
                                      OR (RecordType.DeveloperName = 'EMMEIT'  AND ExternalManagerList__c ='Yes')];
  for (Account ac : aclist1 )
  {
  if(aclist1.size()>0)
  {
  docids.add(ac.id);
  }
  }
  list<Account> aclist =[select id,name,EmergingManager__c,TransitionManager__c,AssetClassString__c,EmergingManagerType__c from Account where id in : docids];
  for(Account ac: aclist ){ 
  if(ac.name != null){
  EMLSnapshot__c  ess = new EMLSnapshot__c();
  ess.Account__c= ac.id;
  ess.EmergingManager__c =ac.EmergingManager__c;
  ess.EmergingManagerType__c  =ac.EmergingManagerType__c;
  ess.TransitionManager__c = ac.TransitionManager__c;
  ess.AssetClass__c = ac.AssetClassString__c;
  Elist.add(ess); 
  }
  }
  insert Elist;
  }
  }