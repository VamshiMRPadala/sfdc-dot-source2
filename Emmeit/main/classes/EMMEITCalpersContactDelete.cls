public without sharing class EMMEITCalpersContactDelete {
    
     @AuraEnabled
  public static String getUserName() {
    return userinfo.getName();
  }
    
@auraEnabled
    public static void  getAllregistrations(string ccid) 
    {
        id uRoleId = UserInfo.getUserRoleId();
        userRole rname=[select id,name from userRole where id=:uRoleId];
        
             String cid=ccid;
        
          list<CalPERSContact__c> cc=[select id,name,AssetClass__c from CalPERSContact__c where id=:cid];
        system.debug('cc============>'+cc);
          
        if(!cc.isempty()){
          /*  if(rname.name==cc[0].AssetClass__c){*/
           delete cc;  
       // }
        }
}
    @auraEnabled
    public static string  ciid() 
    {
        id uRoleId = UserInfo.getUserRoleId();
        userRole rname=[select id,name from userRole where id=:uRoleId];
        string rrname=rname.name;
        return rrname;
    }
     @auraEnabled
    public static string cc(string ccid) 
    {
        String cid=ccid;
        
          CalPERSContact__c cc=[select id,name,AssetClass__c from CalPERSContact__c where id=:cid];
        
        string assname=cc.AssetClass__c;
        return assname;
    
}
}