@isTest(seeAllData = true)
public class EMMEITFundTriggerHelper_Test {
 Static testMethod void EMMEITFundTriggerHelperTest() {
      Test.StartTest();
     User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       system.runAs(thisUser){
      UserRole ur=[SELECT Id, Name from UserRole where DeveloperName ='SystemAdministrator'];
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User();
        u.LastName = 'last';
        u.Profileid = pf.id;
        u.Email = 'puser0001@amamama.com';
        u.Username = 'puser0001@amamama.com';
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'alias';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.UserRoleId=ur.id;
        insert u;    
     User u1 = new User();
        u1.LastName = 'last1';
        u1.Profileid = pf.id;
        u1.Email = 'puser0002@amamama.com';
        u1.Username = 'puser0002@amamama.com';
        u1.CompanyName = 'TEST1';
        u1.Title = 'title1';
        u1.Alias = 'alias1';
        u1.TimeZoneSidKey = 'America/Los_Angeles';
        u1.EmailEncodingKey = 'UTF-8';
        u1.LanguageLocaleKey = 'en_US';
        u1.LocaleSidKey = 'en_US';
        u1.UserRoleId=ur.id;
        insert u1;
     
        User u3 = new User();
        u3.LastName = 'last3';
        u3.Profileid = pf.id;
        u3.Email = 'puser0003@amamama.com';
        u3.Username = 'puser0003@amamama.com';
        u3.CompanyName = 'TEST3';
        u3.Title = 'title3';
        u3.Alias = 'alias3';
        u3.TimeZoneSidKey = 'America/Los_Angeles';
        u3.EmailEncodingKey = 'UTF-8';
        u3.LanguageLocaleKey = 'en_US';
        u3.LocaleSidKey = 'en_US';
        u3.UserRoleId=ur.id;
        insert u3;
     
        User u4 = new User();
        u4.LastName = 'last4';
        u4.Profileid = pf.id;
        u4.Email = 'puser0004@amamama.com';
        u4.Username = 'puser0004@amamama.com';
        u4.CompanyName = 'TEST4';
        u4.Title = 'titl4';
        u4.Alias = 'alias4';
        u4.TimeZoneSidKey = 'America/Los_Angeles';
        u4.EmailEncodingKey = 'UTF-8';
        u4.LanguageLocaleKey = 'en_US';
        u4.LocaleSidKey = 'en_US';
        u4.UserRoleId=ur.id;
        insert u4; 
     
        User u5 = new User();     
        u5.LastName = 'last5';
        u5.Profileid = pf.id;
        u5.Email = 'puser0005@amamama.com';
        u5.Username = 'puser0005@amamama.com';
        u5.CompanyName = 'TEST5';
        u5.Title = 'title5';
        u5.Alias = 'alias5';
        u5.TimeZoneSidKey = 'America/Los_Angeles';
        u5.EmailEncodingKey = 'UTF-8';
        u5.LanguageLocaleKey = 'en_US';
        u5.LocaleSidKey = 'en_US';
        u5.UserRoleId=ur.id;
        insert u5;      
     
        User u6 = new User();
        u6.LastName = 'last6';
        u6.Profileid = pf.id;
        u6.Email = 'puser0006@amamama.com';
        u6.Username = 'puser0006@amamama.com';
        u6.CompanyName = 'TEST6';
        u6.Title = 'title6';
        u6.Alias = 'alias6';
        u6.TimeZoneSidKey = 'America/Los_Angeles';
        u6.EmailEncodingKey = 'UTF-8';
        u6.LanguageLocaleKey = 'en_US';
        u6.LocaleSidKey = 'en_US';
        u6.UserRoleId=ur.id;
        insert u6;

     
        User u7 = new User();

        u7.LastName = 'las7';
        u7.Profileid = pf.id;
        u7.Email = 'puser0007@amamama.com';
        u7.Username = 'puser0007@amamama.com';
        u7.CompanyName = 'TEST7';
        u7.Title = 'title7';
        u7.Alias = 'alias7';
        u7.TimeZoneSidKey = 'America/Los_Angeles';
        u7.EmailEncodingKey = 'UTF-8';
        u7.LanguageLocaleKey = 'en_US';
        u7.LocaleSidKey = 'en_US';
        u7.UserRoleId=ur.id;
        insert u7;
      
     User u8 = new User();

        u8.LastName = 'last8';
        u8.Profileid = pf.id;
        u8.Email = 'puser0008@amamama.com';
        u8.Username = 'puser0008@amamama.com';
        u8.CompanyName = 'TESt8';
        u8.Title = 'title8';
        u8.Alias = 'alias8';
        u8.TimeZoneSidKey = 'America/Los_Angeles';
        u8.EmailEncodingKey = 'UTF-8';
        u8.LanguageLocaleKey = 'en_US';
        u8.LocaleSidKey = 'en_US';
        u8.UserRoleId=ur.id;
        insert u8; 
     
 // User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
   //    system.runAs(thisUser){
     Account at = new Account();
        at.name = 'test';
       
            insert at;
 
     Fund__c f = new Fund__c ();
        f.name = 'test';
        f.Account__c = at.id;
        f.AssetClass__c = 'Real Assets';
        f.SSBCodeCUSIP__c = '09876F456';
      //f.PrimaryMonitor__c = u.id;
       // f.SecondaryMonitor__c=u1.id;
        f.ApprovalStatus__c='Not Started';
      
   try{      insert f; }catch(exception e){}
           
        f.name = 'test';
        f.Account__c = at.id;
        f.AssetClass__c = 'Real Assets';
        f.SSBCodeCUSIP__c = '09876F456';
      //  f.PrimaryMonitor__c = u3.id;
    //  f.SecondaryMonitor__c=u4.id;
        f.ApprovalStatus__c='Not Started';
       f.Status__c='Active';
            try{     update f; }catch(exception e){}
 
/*Primary monitor test ends here*/
/*secondary monitor test starts here*/
       
        Account at1 = new Account();
        at1.name = 'test1';
     
            insert at1;
             Fund__c f1 = new Fund__c ();
        f1.name = 'test';
        f1.Account__c = at1.id;
        f1.AssetClass__c = 'Real Assets';
        f1.SSBCodeCUSIP__c = '09876G428';
     //f1.PrimaryMonitor__c = u5.id;
      // f1.SecondaryMonitor__c=u6.id;
        f1.ApprovalStatus__c='Not Started';
    
       try{      
  insert f1;
           }catch(exception e){}

 
        f1.name = 'test5';
        f1.Account__c = at1.id;
        f1.AssetClass__c = 'Real Assets';
        f1.SSBCodeCUSIP__c = '09876G428';
     // f1.PrimaryMonitor__c = u7.id;
    //  f1.SecondaryMonitor__c=u8.id;
        f1.ApprovalStatus__c='Not Started';
  f1.Status__c='Active';
          try{      update f1; }catch(exception e){}
   
       Fund__c f3 = new Fund__c ();
        f3.name = 'test7';
        f3.Account__c = at.id;
        f3.AssetClass__c = 'Real Assets';
        f3.SSBCodeCUSIP__c = '09876z456';
       //f3.PrimaryMonitor__c = u3.id;
    // f3.SecondaryMonitor__c=u4.id;
        f3.ApprovalStatus__c='Closed';
f3.Status__c='Active';     
         try{      insert f3; }catch(exception e){}
  
     Fund__share fs = new Fund__share();
     fs.ParentId=f.id;
     fs.RowCause='ShareRecordWithSecondaryMonitor__c';
     fs.AccessLevel='Edit';
     fs.UserOrGroupId = u5.id;
                try{      insert fs; }catch(exception e){}
   
     List<Fund__share> fndShareTobeDeleted= [select id,RowCause,ParentId  from fund__share where (RowCause='ShareRecordWithSecondaryMonitor__c'or RowCause='ShareRecordWithPrimaryMonitor__c') and ParentId =: f.id];
     delete fndShareTobeDeleted; 
     
     Fund__share fs1 = new Fund__share();
     fs1.ParentId=f.id;
     fs1.RowCause='ShareRecordWithPrimaryMonitor__c';
     fs1.AccessLevel='Edit';
     fs1.UserOrGroupId = u6.id; 
            try{      insert fs1; }catch(exception e){}
   
     List<Fund__share> fndShareTobeDeleted1= [select id,RowCause,ParentId  from fund__share where (RowCause='ShareRecordWithSecondaryMonitor__c'or RowCause='ShareRecordWithPrimaryMonitor__c') and ParentId =: f.id];
     try{       delete fndShareTobeDeleted1; }catch(exception e){} 
            }
 Test.StopTest();
      
 }
}