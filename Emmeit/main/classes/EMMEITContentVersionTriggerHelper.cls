public class EMMEITContentVersionTriggerHelper {
    
    @future
    public static void createDocLinks(set<ID> docIDs)
    {
        List<ContentDocumentLink > cdllist = new List<ContentDocumentLink>();
    //list all CV's from docid's from contentversion Trigger
      List<ContentVersion> cvList =[select id,Asset_Class__c,ContentDocumentId from ContentVersion where ContentDocumentId IN :docIDs];
    
       set<String> astNames=new   set<String>();
        Map<String, ID> cGroup=new  Map<String, ID>();
         for(ContentVersion cvl: cvList){
             astNames.add(cvl.Asset_Class__c);
         }
         //select cg from assetclass names from the astnames
        system.debug('==astNames==='+astNames);
        for(CollaborationGroup cbg :[select id,Name from CollaborationGroup where Name IN :astNames])
        {
            cGroup.put(cbg.Name, cbg.ID);
        }
         system.debug('==cGroup==='+cGroup);
      for(ContentVersion cvl: cvList){
          system.debug('===cvl.Asset_Class__c==='+cvl.Asset_Class__c);
          // insert CDl for cv's that were queried earlier for assetclass name 
      if(cvl.Asset_Class__c !=Null && cGroup.containsKey(cvl.Asset_Class__c)){
     cvl.SharingPrivacy = 'Private On Records';
    //  cvl.SharingOption = 'Freeze Sharing On'; 
     // update cvl; 
         ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId=cvl.ContentDocumentId;
            if(cdl.LinkedEntityId!=cGroup.get(cvl.Asset_Class__c)){
            cdl.LinkedEntityId = cGroup.get(cvl.Asset_Class__c);
            }
            cdl.ShareType = 'C';
            cdllist.add(cdl);
             system.debug('-cdllist-');
                              
        }
        insert cdllist;
        
    }

}
    Public static void deleteDocLinksOnChange(set<ID> docIDs)
    {
         List<ContentdocumentLink> doclinkstobedeleted = new  List<ContentdocumentLink>();
//list CDL based contentdocumentid in docid's from EMMEITContentVersionTrigger
      for( ContentdocumentLink cdl: [Select id,ContentDocumentId,LinkedEntityId,sharetype from contentdocumentLink where ContentDocumentId IN :docIDs]){
        system.debug('======'+cdl.sharetype);
          If(cdl.sharetype=='c'){
            doclinkstobedeleted.add(cdl) ;            
        }
          system.debug('===22===');
          // Delete the CDL because asset class has changed
        } system.debug('===22==='+doclinkstobedeleted);
        if(!doclinkstobedeleted.isempty()){
        delete doclinkstobedeleted;
        
    } 
    //repeat the process of adding another CG based on the changed asset class
    if(!Test.isRunningTest())
          EMMEITContentVersionTriggerHelper.createDocLinks(docIds);
    }
}