@istest(seealldata=true)
public class EMMEITEMLSnapshotUpdateHelper_Test {
/*static testMethod void EMMEITContentDocumentUpdateHelper(){  
     Test.StartTest();
    
    UserRole ur=[SELECT Id, Name from UserRole where DeveloperName ='RealAssetsRole'];
      Profile pf = [SELECT Id FROM Profile WHERE Name = 'INVO Base User - Light License'];
        User u = new User();
        u.LastName = 'last';
        u.Profileid = pf.id;
        u.Email = 'puser0001@amamama.com';
        u.Username = 'puser0001@amamama.com';
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'alias';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.UserRoleId=ur.id;
        insert u;    
    
     User u1 = new User();
        u1.LastName = 'last1';
        u1.Profileid = pf.id;
        u1.Email = 'puser0002@amamama.com';
        u1.Username = 'puser0002@amamama.com';
        u1.CompanyName = 'TEST1';
        u1.Title = 'title1';
        u1.Alias = 'alias1';
        u1.TimeZoneSidKey = 'America/Los_Angeles';
        u1.EmailEncodingKey = 'UTF-8';
        u1.LanguageLocaleKey = 'en_US';
        u1.LocaleSidKey = 'en_US';
        u1.UserRoleId=ur.id;
        insert u1;
     User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       system.runAs(thisUser){
    
    list<RecordType> rList = [Select id  From RecordType Where sObjectType = 'Account' and RecordType.DeveloperName= 'EMMEIT'];
        // insert parent Account
        system.debug('===nikhil=='+rList[0].id);
        Account ac = new Account();
        ac.name='test';
        ac.RecordTypeId=label.EMMEITAccountRecordType;
        ac.Status__c= 'Active';
        ac.OwnerId=label.EMMEITSystemOwner;
        ac.EmergingManager__c = 'true';
            insert ac;
 
    
        Fund__c f =new Fund__c();
        f.Name='testfund';
        f.Account__c=ac.id;
        f.AssetClass__c='Real Assets';
        f.Status__c='Active';
        f.SSBCodeCUSIP__c  ='0974737N45'; 
        f.PrimaryMonitor__c=u1.id;
        f.SecondaryMonitor__c=u.id;
     
            insert f;
 
      ac.Status__c= 'Active';
      ac.CDR__c ='fgf';
      update ac;
        
    list<Account> aclist =[select id,name,EmergingManager__c from Account where id  =: ac.Id];
    for(Account ac1: aclist ){ 
   if(ac1.name != null){
   EMLSnapshot__c  ess = new EMLSnapshot__c();
   ess.Account__c= ac1.id;
   ess.EmergingManager__c =ac1.EmergingManager__c;
       insert ess;
   }
    
    }
   }
    Test.StopTest();     
}*/

static testMethod void EMMEITContentDocumentUpdateHelper(){  
 String CRON_EXP = '0 0 0 28 2 ? 2022';
EMMEITEMLSnapshotUpdateHelper sh=new EMMEITEMLSnapshotUpdateHelper();

       Id RecordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('EMMEIT').getRecordTypeId();
     
       
        Account ac = new Account();
        ac.name='test';
        //ac.recordTypeid=RecordTypeIdAcc;
        ac.Status__c= 'Active';
      //  ac.OwnerId=label.EMMEITSystemOwner;
        ac.EmergingManager__c = 'true';
        insert ac;
            
            EMMEITEMLSnapshotUpdateHelper.EMLUpdate();

            
            String jobId2 = System.schedule('EMLupdate',
            CRON_EXP,
            new EMMEITEMLSchedule());
            

list<contentdocument> cdlist =[select id,createddate,LatestPublishedVersionId from contentdocument  limit 1];
id rid=cdlist[0].LatestPublishedVersionId;
list<contentversion> cvlist =[select id,createddate from contentversion where id = : rid and Recordtype.Developername='EMMEIT'];

EMMEITContentDocumentUpdateHelper.CVUpdate();




EMMEITContentOwnerSchedule cc=new EMMEITContentOwnerSchedule();

String jobId = System.schedule('cvUpdates',
CRON_EXP,
new EMMEITContentOwnerSchedule());

}


}