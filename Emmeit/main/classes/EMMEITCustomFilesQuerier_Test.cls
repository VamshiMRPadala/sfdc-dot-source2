@isTest
private class EMMEITCustomFilesQuerier_Test{
    static testMethod void createAndDeletecgmLinkstest(){     
     User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       system.runAs(thisUser){ 
         User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'INVO Base User - Platform License'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US', 
     LocaleSidKey = 'en_US',
     UserRoleId = [SELECT Id FROM UserRole WHERE Name = 'Global Equity'].Id);
        insert u ;
      
        
        Account ajar = new Account();
        ajar.name = 'pimco';
        insert ajar;
       
         
        Fund__c f = new Fund__c();
        f.name = 'arampo';
        f.AssetClass__c =  'Global Equity';
        f.Account__c =ajar.id;
        f.SSBCodeCUSIP__c ='7fghdhg';
        f.PrimaryMonitor__c =u.id;
        insert f;
        
        ContentVersion contentVersion1 = new ContentVersion(); 
        contentVersion1.Title = 'Testcontentversionfile1'; 
        contentVersion1.PathOnClient = 'Testing data'; 
        Blob bodyBlob = EncodingUtil.base64Decode('TestFileData1');
        contentVersion1.VersionData = bodyBlob; 
        contentVersion1.IsMajorVersion = false;
        contentVersion1.origin = 'H';
        contentVersion1.Description ='fjghk';
        contentVersion1.Asset_Class__c = 'Global Fixed Income';
        contentVersion1.Document_Type__c =  'Consultant Reports';
        system.debug('Inserting Contentversion.....');
        insert contentVersion1;
        
           List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument ];
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=ajar.id;
        contentlink.ShareType= 'V';        
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
 List<ContentDocument> documents1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument ];
        ContentDocumentLink contentlink1=new ContentDocumentLink();
        contentlink1.LinkedEntityId=f.id;
        //contentlink1.LinkedentityID.type= 'Fund__c';
        contentlink1.ShareType= 'V';        
        contentlink1.ContentDocumentId=documents1[0].Id;
        contentlink1.Visibility = 'AllUsers'; 
        insert contentlink1;

           List <ContentVersion>  cv1 = [select id,title from contentversion where Id = :contentVersion1.id ];
           
           
        EMMEITCustomFilesQuerier.getAllregistrations('Global Fixed Income','Consultant Reports','Testcontentversionfile1',string.valueOf(system.today()),string.valueOf(system.today()),'Account','schroeder' );
             EMMEITCustomFilesQuerier.getAllregistrations('Global Equity','Manager Assessments','Testcontentversionfile2',string.valueOf(system.today()),string.valueOf(system.today()),'Fund','schroeder1' );
        EMMEITCustomFilesQuerier.getAlldocs('Global Fixed Income','Consultant Reports','Testcontentversionfile3','Fund', 'pimco1');
           EMMEITCustomFilesQuerier.getAlldocs('Global Fixed Income','Consultant Reports','Testcontentversionfile3','Account','pimco1');
       
        EMMEITCustomFilesQuerier outerObject = new EMMEITCustomFilesQuerier();
        //outerObject.contentDocLinkAndVersion = new contentDocLinkAndVersion(contentVersion1);
        
       EMMEITCustomFilesQuerier.contentDocLinkAndVersion innerObj = new EMMEITCustomFilesQuerier.contentDocLinkAndVersion();
        innerObj.LinkedEntityType='Fund__c'.replace('__c', '');
       innerObj.LinkedEntityName='ACpartners';
      //     insert innerObj;
           List<EMMEITCustomFilesQuerier.contentDocLinkAndVersion> innobjc = new List<EMMEITCustomFilesQuerier.contentDocLinkAndVersion>();
         EMMEITCustomFilesQuerier.contentDocLinkAndVersion innerObj1 = new EMMEITCustomFilesQuerier.contentDocLinkAndVersion(contentVersion1);
      
       
         
       }
          }
}