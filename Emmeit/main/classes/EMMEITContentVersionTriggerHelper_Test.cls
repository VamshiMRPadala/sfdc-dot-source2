@isTest(Seealldata=true)
private class EMMEITContentVersionTriggerHelper_Test{ 
    
    static testMethod void createAndDeletecgmLinkstest(){ 
        list<CollaborationGroup> cgList = [select id from CollaborationGroup where name='Global Fixed Income'];
        if(cgList .size()==0){
            CollaborationGroup CollaborationGroupObject = new CollaborationGroup();
            CollaborationGroupObject.CollaborationType = 'Public';
            CollaborationGroupObject.Name = 'Global Fixed Income';
            database.insert(CollaborationGroupObject);
        }
              
        ContentVersion contentVersion1 = new ContentVersion(); 
        contentVersion1.Title = 'Testcontentversionfile1'; 
        contentVersion1.PathOnClient = 'Testing data'; 
        Blob bodyBlob = EncodingUtil.base64Decode('TestFileData1');
        contentVersion1.VersionData = bodyBlob; 
        contentVersion1.IsMajorVersion = false;
        contentVersion1.origin = 'H';
        contentVersion1.Description ='fjghk';
        contentVersion1.Asset_Class__c = 'Global Fixed Income';
        system.debug('Inserting Contentversion.....');
        insert contentVersion1;
        
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion1.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument where Id=:contentVersion_2.ContentDocumentId ];
        System.assertEquals(documents.size(), 1);
        System.assertEquals(documents[0].Id, contentVersion_2.ContentDocumentId);
        System.assertEquals(documents[0].LatestPublishedVersionId, contentVersion_2.Id);
        System.assertEquals(documents[0].Title, contentVersion_2.Title);
        
        system.debug('calling deleteDocLinksOnChange.....');
        EMMEITContentVersionTriggerHelper.deleteDocLinksOnChange(new set<Id>{contentVersion_2.ContentDocumentId});
    }
}