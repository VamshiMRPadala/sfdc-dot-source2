@istest
public class EMMEITUpdateCalPERSContactOwner_Test {
static testMethod void EMMEITUpdateCalPERSContactOwner()   {  
      User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       system.runAs(thisUser){

    UserRole ur=[SELECT Id, Name from UserRole where DeveloperName ='SystemAdministrator'];
      Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User();
        u.LastName = 'last';
        u.Profileid = pf.id;
        u.Email = 'puser0001@amamama.com';
        u.Username = 'puser0001@amamama.com';
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'alias';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.UserRoleId=ur.id;
        insert u;    
    
     User u1 = new User();
        u1.LastName = 'last1';
        u1.Profileid = pf.id;
        u1.Email = 'puser0002@amamama.com';
        u1.Username = 'puser0002@amamama.com';
        u1.CompanyName = 'TEST1';
        u1.Title = 'title1';
        u1.Alias = 'alias1';
        u1.TimeZoneSidKey = 'America/Los_Angeles';
        u1.EmailEncodingKey = 'UTF-8';
        u1.LanguageLocaleKey = 'en_US';
        u1.LocaleSidKey = 'en_US';
        u1.UserRoleId=ur.id;
        insert u1;
    // User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
      // system.runAs(thisUser){
    
    list<RecordType> rList = [Select id  From RecordType Where sObjectType = 'Account' and RecordType.DeveloperName= 'EMMEIT'];
        // insert parent Account
        system.debug('===nikhil=='+rList[0].id);
        Account ac = new Account();
        ac.name='test';
        //ac.RecordTypeId='0121D0000004v7m';
        ac.Status__c= 'Active';
      //  ac.OwnerId=label.EMMEITSystemOwner;
      
            insert ac;
 
    
        Fund__c f =new Fund__c();
        f.Name='testfund';
       f.Account__c=ac.id;
        f.AssetClass__c='Real Assets';
        f.Status__c='Active';
        f.SSBCodeCUSIP__c  ='0974737N45'; 
       // f.PrimaryMonitor__c=u1.id;
       // f.SecondaryMonitor__c=u.id;
     
         try{       insert f;}catch(exception e){}
 
      ac.Status__c= 'Active';
      ac.CDR__c ='fgf';
      update ac;
        
    
        string Rc =label.EMMEITAccountRecordType;
        list<RecordType> r1List = [Select id  From RecordType Where sObjectType = 'CalPERSContact__c' and RecordType.DeveloperName= 'EMMEIT' ];
 list<Account> aclist = [Select id,name,RecordType.DeveloperName,Status__c,OwnerId  From Account where id =:ac.id ];
      system.debug('===aclist=='+aclist);
        CalPERSContact__c cc = new CalPERSContact__c();
        cc.Type__c='Primary';
   // cc.RecordTypeId=r1List[0].id;
   cc.User__c=u1.id;
    for (Account a : aclist){
    if(a.RecordType.DeveloperName== 'EMMEIT' && a.Status__c=='Active'){
        cc.Account__c= aclist[0].id;
    }
    }
        insert cc;
 
       }
    
    }
    
}