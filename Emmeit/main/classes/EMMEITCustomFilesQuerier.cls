global with sharing class EMMEITCustomFilesQuerier{
 public map<Id, contentDocLinkAndVersion> cntLinkVrsnMap;
    @auraEnabled
    public static List<contentDocLinkAndVersion> getAllregistrations(string assetclass, string  documenttype, string Title, string startdate, string enddate, string Obj,string Linkedname) 
    {
        system.debug('--assetclass--'+ assetclass);
        system.debug('--documenttype--'+ documenttype);
        system.debug('--documenttype--'+ Title);
        // querying  fields  from content version files. Based on User input filter values
     List<Contentversion> reg=new LIST<Contentversion>();  
       String queryStr = 'SELECT id, Title, Asset_Class__c,Document_Type__c,Description,ContentDocumentId,LastModifiedDate,LastModifiedBy.name,CreatedDate FROM Contentversion where title != null ';
         queryStr = queryStr + ' AND IsLatest  ='+true +'';
        if(assetclass!= ''&& assetclass != null)
        {
           queryStr = queryStr + ' AND Asset_Class__c =\''+assetclass +'\'';
        } 
         if(documenttype!= ''&& documenttype != null)
        {
           queryStr = queryStr + ' AND Document_Type__c =\''+ documenttype +'\'';
        }
        if(Title!= ''&& Title != null) 
        {  
           queryStr = queryStr + ' AND Title LIKE \'%'+ Title +'%\'';
        }
        if(startdate!= ''&& startdate != null)
        {
           queryStr = queryStr + ' AND CreatedDate >= '+ startdate +'T00:00:00.000Z';
        }
          if(enddate!= ''&& enddate != null)
        {
           queryStr = queryStr + ' AND CreatedDate <= '+ enddate +'T00:00:00.000Z';
        }
          // queryStr = queryStr + ' LIMIT 50';
        system.debug('--queryStr--'+ queryStr);
          // Creating a map of contentversion and contentdocumentlink     
        map<Id, contentDocLinkAndVersion> cntLinkVrsnMap=new map<Id, contentDocLinkAndVersion>();
        list<contentDocLinkAndVersion> cntLinkVrsnMap1=new list<contentDocLinkAndVersion>();
        for(Contentversion cvs  :database.query(queryStr)) 
        {
        contentDocLinkAndVersion cdlv=new contentDocLinkAndVersion(cvs);
        cntLinkVrsnMap.Put(cvs.ContentDocumentId, cdlv);
        }
        system.debug('==cntLinkVrsnMap==='+cntLinkVrsnMap.size());
        //Introducing contentdocumentlink values into the map so as to get the column field values on 1 level   
        List<contentdocumentlink> cntDoclimks=getAlldocs(assetclass, documenttype, Title, Obj,Linkedname);
        system.debug('==Sizeofcntdoclinks==='+cntDoclimks.size());
        for(contentdocumentlink cdl:cntDoclimks)
        {      
        if( cntLinkVrsnMap.ContainsKey(cdl.ContentDocumentId) )
        {           
        cntLinkVrsnMap.get(cdl.ContentDocumentId).LinkedEntityType=cdl.Linkedentity.type.replace('__c', '');
        cntLinkVrsnMap.get(cdl.ContentDocumentId).LinkedEntityName=cdl.Linkedentity.Name.replace('__c', '');              
        }                       
        }
       
        system.debug('==cntLinkVrsnMap1==='+cntLinkVrsnMap.Values().size());
       //Verifying if Account or Fund is selected to ensure correct list of contentDocLinkAndVersion are returned      
      for (contentDocLinkAndVersion cdlm : cntLinkVrsnMap.values())
        {
           if(cdlm.LinkedEntityType!= null && cdlm.LinkedEntityType != '' )
        {
               cntLinkVrsnMap1.add(cdlm);
                 system.debug('==cntLinkVrsnMap3==='+cntLinkVrsnMap1);  
        }
        }        
        if (obj == 'Account' || obj == 'Fund' ) 
        {    
            return cntLinkVrsnMap1;
        }
        else 
        {  
            return cntLinkVrsnMap.Values();
        }
        } 
 

 @auraEnabled   
    // Querying  contentdocumentlink for map insertion
    public static List<contentdocumentlink> getAlldocs(string assetclass, string  documenttype, string Title, string Obj,string Linkedname)
    {
       set<String> linked = new set<string>();
        set<id> cvid = new set<id>();   
        for(contentversion cv  : [select id,Asset_class__c,Document_Type__c,Title,ContentDocumentId from Contentversion]){
            cvid.add(cv.ContentDocumentId);
        }
     List<contentdocumentlink> reg1=new LIST<contentdocumentlink>(); 
        List<contentdocumentlink> reg2=new LIST<contentdocumentlink>(); 
        List<contentdocumentlink> reg3=new LIST<contentdocumentlink>();
         List<contentdocumentlink> reg4=new LIST<contentdocumentlink>();
        String st = 'I';
       String queryStr = 'SELECT id, ContentDocumentId,Visibility,ShareType,LinkedEntity.Type,LinkedEntity.Name,LinkedEntityId FROM contentdocumentlink where ContentDocumentId IN :cvid ';       
        system.debug('--queryStr--'+ queryStr);
      // Making sure that colloboration group and user records are not returned 
        reg1=database.query(queryStr);
        for(contentdocumentlink cdl :reg1)
        {
        if ((cdl.LinkedEntity.type != 'CollaborationGroup') && (cdl.LinkedEntity.type != 'User') )
        {
          reg2.add(cdl);
            linked.add(cdl.LinkedEntity.Name);
        }       
        }
       /*for (contentdocumentlink cdl12 : reg2)
        {
            if((Linkedname != '' || Linkedname != null) && linked.contains(Linkedname)){
                reg2.add(cdl12);
            }
            
        }*/
        //Returning list of records that fall under below given criteria
      for (contentdocumentlink cdl1 :reg2)
        {
          if (cdl1.LinkedEntity.type =='Account' && obj=='Account')
           {       
          reg3.add(cdl1);
         }
          else  if (cdl1.LinkedEntity.type =='Fund__c' && obj=='Fund')
            {       
           reg3.add(cdl1);
          }
       }
        if (obj == 'Account' || obj =='Fund' )
        {
            return reg3; 
        }
        else
        {
            return reg2;
        }
    }
  /*  @auraEnabled (Code maybe useful for future enhacement for EMMEIT Files querier) by Nikhil Pulimamidi 12/12/2018
  public static String getAccountName(string assetclass, string  documenttype, string Title)
   {
       string docid;
        String name;
       String cid;
     set<string> mySet = new Set<String>();
        set<id> cvid = new set<id>();
        set<id> cdid = new set<id>();
        for(contentversion cv  : [select id,Asset_class__c,Document_Type__c,Title,ContentDocumentId from Contentversion]){
            cvid.add(cv.ContentDocumentId);
        }
        system.debug('--cvid--'+  cvid);
       for(contentdocumentlink cd : [SELECT id, ContentDocumentId,Visibility,ShareType,LinkedEntityId,Linkedentity.type  FROM contentdocumentlink where ContentDocumentId IN :cvid]){
          cid = cd.LinkedEntity.type;
           mySet.add(cd.Linkedentity.type );
           cdid.add(cd.LinkedEntityId);
       }
       system.debug('--cdid--'+  cdid);
       system.debug('--cid--'+  cid);
       list<contentdocumentlink> cdll =[select id, ContentDocumentId,Visibility,ShareType,LinkedEntityId,Linkedentity.type from contentdocumentlink where LinkedentityId In : (cdid)];
       for(contentdocumentlink cd : cdll)
       {
          if (cd.Linkedentity.type == 'Account')
           {
          //  name = [select Name from Account where Id = :cd.LinkedEntityId].get(0).Name; 
               name = 'Account';
               docid=cd.ContentDocumentId;
                 system.debug('--name--'+  name); 
                system.debug('--docid--'+  docid); 
           } 
           else if (cd.Linkedentity.type == 'CollaborationGroup')
           {
           // name = [select Name from CollaborationGroup where Id = :cd.LinkedEntityId].get(0).Name; 
            name = 'CollaborationGroup';
               docid=cd.ContentDocumentId;
                system.debug('--name--'+  name);  
                system.debug('--docid--'+  docid); 
           } 
            else if (cd.Linkedentity.type == 'User')
           {
            //name = [select Name from User where Id = :cd.LinkedEntityId].get(0).Name; 
                name = 'User';
               docid=cd.ContentDocumentId;
             system.debug('--name--'+  name);  
                 system.debug('--docid--'+  docid); 
           } 
       }
        return name;
   }
    public Contentversion getSelectedregistrations(Id id)
    {    
      Contentversion  reg=[select id,Asset_class__c,Document_Type__c,Title,ContentDocumentId from Contentversion where id=:id];
        return reg;
    }*/
 
    public class contentDocLinkAndVersion
    {
     public  contentDocLinkAndVersion(){}   
      @AuraEnabled public string LinkedEntityType{get ; set ;}
      @AuraEnabled public string LinkedEntityName{get ; set ;}    
        //@AuraEnabled public string Lid{get ; set ;}      
      @AuraEnabled  public contentversion cnttversion{get ; set ;}
        public contentDocLinkAndVersion (contentversion ctnv){
        cnttversion=ctnv;
        
        }

    }
   
}