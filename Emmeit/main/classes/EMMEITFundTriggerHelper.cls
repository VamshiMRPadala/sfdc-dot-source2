public class EMMEITFundTriggerHelper { 
    Public static void removeSharingPermission(List<Fund__c> Newfnds, Map<ID, Fund__c> oldFndsMap)
    {
        Set<ID> fundUserChngID=new  Set<ID> ();
        //get all fundid's for all trigger scenario's i.e if PM changed or SM changed or Fund Status is closed
        for(Fund__c fnd: Newfnds)
        {
            if((fnd.SecondaryMonitor__c !=oldFndsMap.get(fnd.ID).SecondaryMonitor__c || fnd.PrimaryMonitor__c !=oldFndsMap.get(fnd.ID).PrimaryMonitor__c || fnd.ApprovalStatus__c == 'Closed' ) )
            {
                fundUserChngID.add(fnd.ID);
            }
        }
     /*   List<Fund__c> FundsToUpdate = new List<Fund__c>();
        List<Fund__c> FundsList= [select id,name,RecordtypeId,ownerId from Fund__c where Recordtype.developername='EMMEIT'];
        for (fund__c fnd : FundsList)
        {
           
              fnd.ownerid =label.EMMEITSystemOwner;
              FundsToUpdate.add(fnd);
            
        }
        update FundsToUpdate;*/
    //List fundshare based on parentid's in fundid's and delete them
List<Fund__share> fndShareTobeDeleted= [select id,RowCause,ParentId  from fund__share where (RowCause='ShareRecordWithSecondaryMonitor__c'or RowCause='ShareRecordWithPrimaryMonitor__c') and ParentId IN : fundUserChngID];
 if(!fndShareTobeDeleted.IsEmpty() ) {
   delete fndShareTobeDeleted;            
  }
}
        

}