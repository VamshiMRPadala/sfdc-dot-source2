public class EMMEITContentDocumentUpdateHelper {
public static void CVUpdate(){
//String S1 = System.Label.EMMEITContentDocumentTimeICOR;
//integer T1 = integer.ValueOf(S1);
String S = System.Label.EMMEITContentDocumentTime;
integer T = integer.ValueOf(S);
   set<Id> docIds=new set<Id>();
   //list all CD's and put that value of latestpublishedversionId into a set
   list<contentdocument> cdlist =[select id,createddate,LatestPublishedVersionId from contentdocument];
     for(ContentDocument cd: cdlist){ 
     docIds.add(cd.LatestPublishedVersionId);
     }
 //use the set of latestpublishedversionId as id for querying contentversion and update owner of that content version which inturn changes the owner of CD itself
 List<contentversion>  cvlist1 = new List<contentversion>();
      list<contentversion> cvlist =[select id,OwnerChangeDate__c,createddate,Recordtype.Developername from contentversion where id = : docIds and (Recordtype.Developername='EMMEIT' OR Recordtype.Developername='EntityMaster')];
     for(contentversion cv : cvlist){
     if(Datetime.Now() >= cv.OwnerChangeDate__c){
     cv.ownerId = label.EMMEITSystemOwner;
    cvlist1.add(cv);
     }
     }
   try{ Update cvlist1 ;}catch(exception e){}
     }
     }