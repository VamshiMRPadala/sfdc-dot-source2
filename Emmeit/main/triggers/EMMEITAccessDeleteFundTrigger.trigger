trigger EMMEITAccessDeleteFundTrigger on Fund__c (After Update,Before insert) {

    for (fund__c fnd : Trigger.new)
    {
    //Changing the owner for all new Funds 
       if(Trigger.IsBefore){
         if(fnd.Recordtype.developername=='EMMEIT')
          fnd.assetclass__c = fnd.assetclass__c ;
          fnd.ownerid =label.EMMEITSystemOwner;
        }
    }
   
// For deleting permisssion for PM & SM
if(Trigger.isUpdate){
EMMEITFundTriggerHelper.removeSharingPermission(Trigger.new, Trigger.oldMap);
}
}