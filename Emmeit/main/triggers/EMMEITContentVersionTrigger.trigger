trigger EMMEITContentVersionTrigger on ContentVersion (Before Insert,After Insert,After Update) {


    set<Id> docIds=new set<Id>();
    for (ContentVersion  cv : trigger.new){
    //Get fund id's for all trigger scenarios(I.e where asset class changed and new fund insertion)
            if(Trigger.IsInsert || ( Trigger.IsUpdate&& cv.Asset_Class__c !=Trigger.oldMap.get(cv.ID).Asset_Class__c))         
            {
              //cv.SharingOption = 'Freeze Sharing On';  
                docIds.add(cv.ContentDocumentId);
            }
            if(Trigger.IsBefore && Trigger.IsInsert)
            {
            cv.SharingOption='R';
            cv.SharingPrivacy='P';   
            }
            }
        
    if(Trigger.IsInsert) 
   EMMEITContentVersionTriggerHelper.createDocLinks(docIds);
    
    if(Trigger.IsUpdate && !docIds.IsEmpty()) 
   EMMEITContentVersionTriggerHelper.deleteDocLinksOnChange(docIds);
    

}