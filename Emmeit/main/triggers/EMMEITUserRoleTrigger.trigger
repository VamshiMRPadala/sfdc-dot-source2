trigger EMMEITUserRoleTrigger on User (After Insert,After Update) {
  set<Id> usIds=new set<Id>();
  //get usids for trigger scenarios (trigger isiinsert and trigger isupdate)
    for (User  ur : trigger.new){
            if(Trigger.IsInsert || ( Trigger.IsUpdate && ur.UserRoleId!=Trigger.oldMap.get(ur.id).UserRoleId))
            {
                //ur.Department = ur.UserRole;
                usIds.add(ur.id);
            }
        }
        
    if(Trigger.IsInsert) 
    EMMEITCollaborationMemberTriggerHelper.createcgmLinks(usIds);
    
    if(Trigger.IsUpdate && !usIds.IsEmpty()) 
   EMMEITCollaborationMemberTriggerHelper.deletecgmOnChange(usIds);
    

}